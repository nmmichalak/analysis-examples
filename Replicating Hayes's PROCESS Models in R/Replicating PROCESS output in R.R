library(MBESS)
library(Hmisc)
library(lavaan)

med.data <- spss.get(file = "~/Downloads/mediation_data.sav",
                     lowernames = TRUE,
                     to.data.frame = TRUE)

center.var <- function(x){
  centered <- as.vector(scale(x = x, center = TRUE,
                              scale = FALSE))
  return(centered)
}

med.data[,c("math_c", "read_c", "write_c", "socst_c")] <- lapply(X = med.data[,c("math", "read", "write", "socst")], FUN = function(x) center.var(x))

med1 <- mediation(x = med.data$math_c,
          mediator = med.data$read_c,
          dv = med.data$science,
          conf.level = .95,
          B = 1000, which.boot = "Bca",
          bootstrap = TRUE,
          save.bs.replicates = TRUE)

hayes4 <- ' # direct effect
              science ~ c*math_c
              direct := c

            # mediator
              read_c ~ a*math_c
              science ~ b*read_c

            # indirect effect (a*b)
              indirect := a*b

            # total effect
              total := c + (a*b)'

sem1 <- sem(model = hayes4,
            data = med.data,
            se = "bootstrap",
            bootstrap = 1000)

summary(sem1,
        fit.measures=TRUE,
        standardize=TRUE,
        rsquare=TRUE)

parameterEstimates(sem1,
                   boot.ci.type="bca.simple",
                   level=0.95, ci=TRUE,
                   standardized = FALSE)

# moderated mediation
# process vars = hisci read math write

# have to create interaction term seperately
med.data$math_c.write_c <- with(med.data, math_c*write_c)

hayes7 <- '
# regressions
read_c ~ a1*math_c
science ~ b1*read_c
read_c ~ a2*write_c
read_c ~ a3*math_c.write_c
science ~ cdash*math_c

# indirect effects conditional on moderator (a1 + a3*a2.value)*b1
indirect.below := a1*b1 + a3*-9.478586*b1
indirect.mean := a1*b1 + a3*0*b1
indirect.above := a1*b1 + a3*9.478586*b1
'
sem2 <- sem(model = hayes7,
            data = med.data,
            se = "bootstrap",
            bootstrap = 1000)

summary(sem2,
        fit.measures=TRUE,
        standardize=TRUE,
        rsquare=TRUE)

parameterEstimates(sem2,
                   boot.ci.type="bca.simple",
                   level=0.95, ci=TRUE,
                   standardized = FALSE)

# create interaction term
med.data$socst_c.math_c <- with(med.data, socst_c*math_c)

hayes1 <- '# regressions
           read ~ b1*socst_c
           read ~ b2*math_c
           read ~ b3*socst_c.math_c
           
           # mean of centered math (for use in simple slopes)
           math_c ~ math.mean*1

           # variance of centered math (for use in simple slopes)
           math_c ~~ math.var*math_c

           # simple slopes
           SD.below := b1 + b3*(math.mean - sqrt(math.var))
           mean := b1 + b3*(math.mean)
           SD.above := b1 + b3*(math.mean + sqrt(math.var))
           '

sem3 <- sem(model = hayes1,
            data = med.data,
            se = "bootstrap",
            bootstrap = 1000)

summary(sem3,
        fit.measures=TRUE,
        standardize=TRUE,
        rsquare=TRUE)

parameterEstimates(sem3,
                   boot.ci.type="bca.simple",
                   level=0.95, ci=TRUE,
                   standardized = FALSE)

summary(lm(read ~ socst_c*math_c, med.data))

library(ggplot2)
med.data$math.grps <- factor(with(med.data,
                           ifelse(math_c < -sd(math_c), "-1SD",
                           ifelse(math_c > sd(math_c), "+1SD",
                           ifelse(is.na(math_c) == FALSE, "mean", NA)))))

med.data$math.grps <- with(med.data,
                           factor(math.grps,
                                  levels = levels(math.grps)[c(1, 3, 2)]))
  
ggplot(med.data[med.data$math.grps != "mean",], aes(x = socst_c, y = read, color = math.grps)) +
  geom_point() + geom_smooth(method = "lm") + scale_color_brewer(palette = "Set1")