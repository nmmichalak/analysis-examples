# Replicating Hayes’s PROCESS Models in R
Nick Michalak  
July 28, 2016  

> ### If you're an experimental psychologist, you likely know that journals expect more than "main effects" or "one cause fits all" explanations of psychological phenomenon. That is, they expect any given phenomenon to "depend on" some circumstance or some trait, or they expect there to exist some process by which the phenomenon occurs, where first X happens, which leads to M followed by Y.

> ### In the social sciences, researchers overhwelmingly prefer two statistical methods for demonstrating such psychological processes: moderation and mediation. Since _at least_ 1986 when Baron and Kenny^1^ explained the difference, researchers have gone sorta hog-wild in shoehorning every hypothesis into a moderation and/or mediation framework. The statistical techniques for testing these relationships are so popular that I'd bet most empirical articles published in the past 20 years are no more than 1 degree removed from a reference to Baron & Kenny (1986), Aiken, West, & Reno (1991)^2^, or Preacher & Hayes (2008)^3^.

> ### This post is about that last reference, specifically Hayes who had the brilliant idea to code and release (for free) macros that add user-friendly dropdown menus for conducting not 1, 2, but 76 statistical models in SPSS and SAS. If you're a graduate student or "younger" faculty member, you're probably familiar with at least 2 of these models: Model 1 for moderation and Model 4 for mediation.

> ### I'm going to show you how to replicate their output in R. It's my hope that learning how to test these models in R will give you a framework for testing similar or even more complex models.


### Here's where to find the data: [UCLA SPSS Statistical Computing Workshop Moderation and Mediation using the Process Macro](http://www.ats.ucla.edu/stat/spss/seminars/process_macro/process_training.htm)

### Hayes's PROCESS Model 1 moderation with a continuous moderator (see Hayes's template for all 76 models [here](http://afhayes.com/public/templates.pdf))

### Here's how you do it in SPSS

> ### [Donwload](http://processmacro.org/download.html) the folder with the PROCESS macros and documentation. Open then file called 'process.sps', select everything in the syntax window and click the giant green arrow to run. Then open a new syntax file.

![](/Users/nmmichalak/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/hayes model 1.png) 

get
file = YOUR.FILE.PATH.HERE.

PROCESS vars = socst read math

/ x = socst

/ m = math

/ y = read

/ model = 1

/ center = 1

/ boot = 1000.

### Here's how you do it in R

### Read in the data


```r
setwd(dir = "~/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/")

want_packages <- c("Hmisc", "lavaan", "knitr")
have_packages   <- want_packages %in% rownames(installed.packages())
if(any(!have_packages)) install.packages(want_packages[!have_packages])
library(Hmisc)

egdata <- spss.get(file = "~/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/mediation_data.sav",
                     lowernames = TRUE,
                     to.data.frame = TRUE)
```

### Functions used
* [`library()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/library.html) activates a package in your library. Think of it as taking a package of the shelf.
* [`installed.packages()`](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/installed.packages.html) gives you a list of all installed packages
* [`install.packages()`](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/install.packages.html) downloads the name of the package you give it (R has so many packages and people develop more every day) and puts it in your library
* [`%in%`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/match.html) matches the names of elements in one vector with the names of elements in another
* [`any()`]() look at this/these logical vector(s) (TRUES and FALSES): is at least one of the values true?
* [`if()`]() use this for executing functions using if-then logic 
* [`spss.get()`]() for reading in SPSS files. I like this function more than the `foreign` package's `read.spss()` function just because it has more features

### Packages used
* [_Hmisc_](https://cran.r-project.org/web/packages/Hmisc/Hmisc.pdf) is a grab bag of functions for data analysis and management (e.g., `spss.get()`, but also way more cool stuff)

### Center variables


```r
# function for centering variables
center.var <- function(x){
  centered <- as.vector(scale(x = x, center = TRUE,
                              scale = FALSE))
  return(centered)
}

# apply function to many variables and name them oldname_c (_c for centered)
egdata[,c("math_c", "read_c", "write_c", "socst_c")] <- lapply(X = egdata[,c("math", "read", "write", "socst")], FUN = function(x) center.var(x))

# means of the vars
sapply(egdata[,c("math", "math_c", "read", "read_c", "write", "write_c", "socst", "socst_c")],
       FUN = function(x){
         round(mean(x), 2)
       }, simplify = TRUE,
       USE.NAMES = TRUE)
```

```
##    math  math_c    read  read_c   write write_c   socst socst_c 
##   52.65    0.00   52.23    0.00   52.77    0.00   52.41    0.00
```

```r
# sd of the vars
sapply(egdata[,c("math", "math_c", "read", "read_c", "write", "write_c", "socst", "socst_c")],
       FUN = function(x){
         round(sd(x), 2)
       }, simplify = TRUE,
       USE.NAMES = TRUE)
```

```
##    math  math_c    read  read_c   write write_c   socst socst_c 
##    9.37    9.37   10.25   10.25    9.48    9.48   10.74   10.74
```

### Functions used
* [`lapply()`](http://www.inside-r.org/r-doc/base/lapply) takes a vector and applys a function to each element of that vector; then it returns a list of each output
* [`sapply()`](http://www.inside-r.org/r-doc/base/lapply) like lapply above by with some more features like simplify (basically trys to make the output prettier) and USE.NAMES (names each output with the names of the input)
* [`function()`]() use this to make your own function

### Hayes's PROCESS Model 1 results for moderation (continuous moderator)

> ### The code is tricky at first but I think it's way easier to learn than Stata or MPLUS (yes, AMOS is easier, like Microsoft paint for SEM). I suggest looking over the basic tutorials on [lavaan's webpage](http://lavaan.ugent.be/tutorial/syntax1.html). For now, fitting and running a model takes 2 steps:

### 1. write the model using regression syntax (~ means =), making sure to name coefficients (`a1*variablename` means the coeff for variablename will be called `a1`) and new paramters (`indirect := a1 * b1` will give you an output line for a1 x b1 called `indirect`)

### 2. run that model using `sem()` or `cfa()`, making sure to give the function the name of the dataframe with your variables

> ### The new part is writting a model as a character element and using that as an argument in the lavaan functions. More new parts are the lavaan operators. They're easy to learn, I think.

![](/Users/nmmichalak/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/hayes model 1.png) 


```r
library(lavaan)
library(knitr)

# create interaction term between X (socst) and M (math)
egdata$socst_c.math_c <- with(egdata,
                              socst_c*math_c)

# parameters
hayes1 <- '# regressions
           read ~ b1*socst_c
           read ~ b2*math_c
           read ~ b3*socst_c.math_c
           
           # mean of centered math (for use in simple slopes)
           math_c ~ math.mean*1

           # variance of centered math (for use in simple slopes)
           math_c ~~ math.var*math_c

           # simple slopes
           SD.below := b1 + b3*(math.mean - sqrt(math.var))
           mean := b1 + b3*(math.mean)
           SD.above := b1 + b3*(math.mean + sqrt(math.var))
           '

# fit the model (this takes some time)
sem1 <- sem(model = hayes1,
            data = egdata,
            se = "bootstrap",
            bootstrap = 1000)

# fit measures
summary(sem1,
        fit.measures = TRUE,
        standardize = TRUE,
        rsquare = TRUE)
```

```
## lavaan (0.5-20) converged normally after  17 iterations
## 
##   Number of observations                           200
## 
##   Estimator                                         ML
##   Minimum Function Test Statistic               76.103
##   Degrees of freedom                                 2
##   P-value (Chi-square)                           0.000
## 
## Model test baseline model:
## 
##   Minimum Function Test Statistic              233.790
##   Degrees of freedom                                 5
##   P-value                                        0.000
## 
## User model versus baseline model:
## 
##   Comparative Fit Index (CFI)                    0.676
##   Tucker-Lewis Index (TLI)                       0.190
## 
## Loglikelihood and Information Criteria:
## 
##   Loglikelihood user model (H0)              -3354.138
##   Loglikelihood unrestricted model (H1)      -3316.086
## 
##   Number of free parameters                          7
##   Akaike (AIC)                                6722.276
##   Bayesian (BIC)                              6745.364
##   Sample-size adjusted Bayesian (BIC)         6723.187
## 
## Root Mean Square Error of Approximation:
## 
##   RMSEA                                          0.430
##   90 Percent Confidence Interval          0.351  0.516
##   P-value RMSEA <= 0.05                          0.000
## 
## Standardized Root Mean Square Residual:
## 
##   SRMR                                           0.180
## 
## Parameter Estimates:
## 
##   Information                                 Observed
##   Standard Errors                            Bootstrap
##   Number of requested bootstrap draws             1000
##   Number of successful bootstrap draws            1000
## 
## Regressions:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   read ~                                                                
##     socst_c   (b1)    0.374    0.052    7.137    0.000    0.374    0.437
##     math_c    (b2)    0.481    0.062    7.803    0.000    0.481    0.490
##     scst_c.m_ (b3)    0.011    0.005    2.293    0.022    0.011    0.118
## 
## Intercepts:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     math_c  (mth.)   -0.000    0.671   -0.000    1.000   -0.000   -0.000
##     read             51.615    0.577   89.476    0.000   51.615    5.628
## 
## Variances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     math_c  (mth.)   87.329    7.198   12.133    0.000   87.329    1.000
##     read             47.473    4.520   10.504    0.000   47.473    0.564
## 
## R-Square:
##                    Estimate
##     read              0.436
## 
## Defined Parameters:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     SD.below          0.268    0.065    4.126    0.000    0.268    0.319
##     mean              0.374    0.053    7.080    0.000    0.374    0.437
##     SD.above          0.479    0.074    6.453    0.000    0.479    0.554
```

```r
# bootstraps

parameterEstimates(sem1,
                   boot.ci.type = "bca.simple",
                   level = .95,
                   ci = TRUE,
                   standardized = FALSE)
```

```
##               lhs op                              rhs     label      est
## 1            read  ~                          socst_c        b1    0.374
## 2            read  ~                           math_c        b2    0.481
## 3            read  ~                   socst_c.math_c        b3    0.011
## 4          math_c ~1                                  math.mean    0.000
## 5          math_c ~~                           math_c  math.var   87.329
## 6            read ~~                             read             47.473
## 7         socst_c ~~                          socst_c            114.681
## 8         socst_c ~~                   socst_c.math_c            -88.161
## 9  socst_c.math_c ~~                   socst_c.math_c           9184.507
## 10           read ~1                                              51.615
## 11        socst_c ~1                                               0.000
## 12 socst_c.math_c ~1                                              54.489
## 13       SD.below := b1+b3*(math.mean-sqrt(math.var))  SD.below    0.268
## 14           mean :=                b1+b3*(math.mean)      mean    0.374
## 15       SD.above := b1+b3*(math.mean+sqrt(math.var))  SD.above    0.479
##       se      z pvalue ci.lower ci.upper
## 1  0.052  7.137  0.000    0.281    0.488
## 2  0.062  7.803  0.000    0.355    0.601
## 3  0.005  2.293  0.022    0.002    0.021
## 4  0.671  0.000  1.000   -1.323    1.375
## 5  7.198 12.133  0.000   74.200  102.506
## 6  4.520 10.504  0.000   39.985   59.063
## 7  0.000     NA     NA  114.681  114.681
## 8  0.000     NA     NA  -88.161  -88.161
## 9  0.000     NA     NA 9184.507 9184.507
## 10 0.577 89.476  0.000   50.579   52.880
## 11 0.000     NA     NA    0.000    0.000
## 12 0.000     NA     NA   54.489   54.489
## 13 0.065  4.126  0.000    0.152    0.430
## 14 0.053  7.080  0.000    0.282    0.491
## 15 0.074  6.453  0.000    0.341    0.624
```

### Functions used
* [`with()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/with.html) I use this function when I don't want to call a dataframe everytime I name a variable (i.e., mydata$myvariable)
* [`$`]() extracts an element from an object
* [`sem()`]() structural equation model fit function
* [`summary()`]() summarises objects like linear models or dataframes and more
* ['parameterEstimates()']() outputs parameter estimates from an sem() or cfa() model

### Packages used
* [_lavaan_]() badass package for structural equation modeling; it's like MPLUS but free, easier to use, and prettier

### Hayes's PROCESS Model 4 "classic" mediation

### Here's how you do it in SPSS

![](/Users/nmmichalak/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/hayes model 4.png) 


PROCESS vars = science read math

/ x = math

/ m = read

/ y = science

/ model = 4

/ center = 1

/ total = 1

/ effsize = 1

/ boot = 1000.

### Here's how you do it in R

### Replicating Hayes's PROCESS Model 4 results for "classic" mediation


```r
# parameters
hayes4 <- ' # direct effect
              science ~ c*math_c
              direct := c

            # regressions
              read_c ~ a*math_c
              science ~ b*read_c

            # indirect effect (a*b)
              indirect := a*b

            # total effect
              total := c + (a*b)'

# fit model
sem2 <- sem(model = hayes4,
            data = egdata,
            se = "bootstrap",
            bootstrap = 1000)
# fit measures
summary(sem2,
        fit.measures = TRUE,
        standardize = TRUE,
        rsquare = TRUE)
```

```
## lavaan (0.5-20) converged normally after  14 iterations
## 
##   Number of observations                           200
## 
##   Estimator                                         ML
##   Minimum Function Test Statistic                0.000
##   Degrees of freedom                                 0
## 
## Model test baseline model:
## 
##   Minimum Function Test Statistic              245.569
##   Degrees of freedom                                 3
##   P-value                                        0.000
## 
## User model versus baseline model:
## 
##   Comparative Fit Index (CFI)                    1.000
##   Tucker-Lewis Index (TLI)                       1.000
## 
## Loglikelihood and Information Criteria:
## 
##   Loglikelihood user model (H0)              -2098.582
##   Loglikelihood unrestricted model (H1)      -2098.582
## 
##   Number of free parameters                          5
##   Akaike (AIC)                                4207.164
##   Bayesian (BIC)                              4223.656
##   Sample-size adjusted Bayesian (BIC)         4207.816
## 
## Root Mean Square Error of Approximation:
## 
##   RMSEA                                          0.000
##   90 Percent Confidence Interval          0.000  0.000
##   P-value RMSEA <= 0.05                          1.000
## 
## Standardized Root Mean Square Residual:
## 
##   SRMR                                           0.000
## 
## Parameter Estimates:
## 
##   Information                                 Observed
##   Standard Errors                            Bootstrap
##   Number of requested bootstrap draws             1000
##   Number of successful bootstrap draws             999
## 
## Regressions:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   science ~                                                             
##     math_c     (c)    0.402    0.087    4.638    0.000    0.402    0.380
##   read_c ~                                                              
##     math_c     (a)    0.725    0.057   12.736    0.000    0.725    0.662
##   science ~                                                             
##     read_c     (b)    0.365    0.078    4.659    0.000    0.365    0.378
## 
## Variances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     science          50.894    4.966   10.249    0.000   50.894    0.522
##     read_c           58.719    5.590   10.504    0.000   58.719    0.561
## 
## R-Square:
##                    Estimate
##     science           0.478
##     read_c            0.439
## 
## Defined Parameters:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     direct            0.402    0.087    4.635    0.000    0.402    0.380
##     indirect          0.265    0.057    4.654    0.000    0.265    0.251
##     total             0.667    0.059   11.250    0.000    0.667    0.631
```

```r
# bootstraps
parameterEstimates(sem2,
                   boot.ci.type = "bca.simple",
                   level = .95, ci = TRUE,
                   standardized = FALSE)
```

```
##        lhs op     rhs    label    est    se      z pvalue ci.lower
## 1  science  ~  math_c        c  0.402 0.087  4.638      0    0.228
## 2   read_c  ~  math_c        a  0.725 0.057 12.736      0    0.606
## 3  science  ~  read_c        b  0.365 0.078  4.659      0    0.222
## 4  science ~~ science          50.894 4.966 10.249      0   42.154
## 5   read_c ~~  read_c          58.719 5.590 10.504      0   48.378
## 6   math_c ~~  math_c          87.329 0.000     NA     NA   87.329
## 7   direct :=       c   direct  0.402 0.087  4.635      0    0.228
## 8 indirect :=     a*b indirect  0.265 0.057  4.654      0    0.162
## 9    total := c+(a*b)    total  0.667 0.059 11.250      0    0.527
##   ci.upper
## 1    0.560
## 2    0.829
## 3    0.534
## 4   61.558
## 5   70.905
## 6   87.329
## 7    0.560
## 8    0.387
## 9    0.770
```

### Hayes's PROCESS Model 7 moderated mediation with a continuous moderator

### Here's how you do it in SPSS

![](/Users/nmmichalak/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/hayes model 7.png) 


PROCESS vars = science read math write

/ x = math

/ m = read

/ w = write

/ y = science

/ model = 7

/ center = 1

/ total = 1

/ boot = 1000.

### Here's how you do it in R

### Replicating Hayes's PROCESS Model 7 results for moderated mediation


```r
# create interaction term between X (math) and M (write)
egdata$math_c.write_c <- with(egdata, math_c*write_c)

hayes7 <- ' # regressions
              read_c ~ a1*math_c
              science ~ b1*read_c
              read_c ~ a2*write_c
              read_c ~ a3*math_c.write_c
              science ~ cdash*math_c

            # mean of centered write (for use in simple slopes)
              write_c ~ write.mean*1

            # variance of centered write (for use in simple slopes)
              write_c ~~ write.var*write_c

            # Index of moderated mediation
              index.mod.med := a3*b1

            # indirect effects conditional on moderator (a1 + a3*a2.value)*b1
              indirect.SDbelow := a1*b1 + a3*-sqrt(write.var)*b1
              indirect.mean := a1*b1 + a3*write.mean*b1
              indirect.SDabove := a1*b1 + a3*sqrt(write.var)*b1'

# fit model
sem3 <- sem(model = hayes7,
            data = egdata,
            se = "bootstrap",
            bootstrap = 1000)

# fit measures
summary(sem3,
        fit.measures = TRUE,
        standardize = TRUE,
        rsquare = TRUE)
```

```
## lavaan (0.5-20) converged normally after  26 iterations
## 
##   Number of observations                           200
## 
##   Estimator                                         ML
##   Minimum Function Test Statistic              119.179
##   Degrees of freedom                                 4
##   P-value (Chi-square)                           0.000
## 
## Model test baseline model:
## 
##   Minimum Function Test Statistic              386.820
##   Degrees of freedom                                 9
##   P-value                                        0.000
## 
## User model versus baseline model:
## 
##   Comparative Fit Index (CFI)                    0.695
##   Tucker-Lewis Index (TLI)                       0.314
## 
## Loglikelihood and Information Criteria:
## 
##   Loglikelihood user model (H0)              -3978.754
##   Loglikelihood unrestricted model (H1)      -3919.164
## 
##   Number of free parameters                         11
##   Akaike (AIC)                                7979.508
##   Bayesian (BIC)                              8015.789
##   Sample-size adjusted Bayesian (BIC)         7980.940
## 
## Root Mean Square Error of Approximation:
## 
##   RMSEA                                          0.379
##   90 Percent Confidence Interval          0.323  0.440
##   P-value RMSEA <= 0.05                          0.000
## 
## Standardized Root Mean Square Residual:
## 
##   SRMR                                           0.199
## 
## Parameter Estimates:
## 
##   Information                                 Observed
##   Standard Errors                            Bootstrap
##   Number of requested bootstrap draws             1000
##   Number of successful bootstrap draws            1000
## 
## Regressions:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   read_c ~                                                              
##     math_c    (a1)    0.507    0.074    6.816    0.000    0.507    0.511
##   science ~                                                             
##     read_c    (b1)    0.365    0.075    4.888    0.000    0.365    0.358
##   read_c ~                                                              
##     write_c   (a2)    0.340    0.070    4.833    0.000    0.340    0.347
##     mth_c._   (a3)    0.004    0.006    0.736    0.462    0.004    0.039
##   science ~                                                             
##     math_c  (cdsh)    0.402    0.084    4.793    0.000    0.402    0.397
## 
## Intercepts:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     write_c (wrt.)    0.000    0.625    0.000    1.000    0.000    0.000
##     read_c           -0.245    0.617   -0.397    0.691   -0.245   -0.026
##     science          51.850    0.496  104.621    0.000   51.850    5.477
## 
## Variances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     write_c (wrt.)   89.394    6.836   13.076    0.000   89.394    1.000
##     read_c           52.636    4.873   10.802    0.000   52.636    0.612
##     science          50.894    5.161    9.861    0.000   50.894    0.568
## 
## R-Square:
##                    Estimate
##     read_c            0.388
##     science           0.432
## 
## Defined Parameters:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     index.mod.med     0.002    0.002    0.734    0.463    0.002    0.014
##     indirect.SDblw    0.170    0.052    3.290    0.001    0.170    0.169
##     indirect.mean     0.185    0.042    4.384    0.000    0.185    0.183
##     indirect.SDabv    0.201    0.042    4.739    0.000    0.201    0.197
```

```r
# bootstraps
parameterEstimates(sem3,
                   boot.ci.type = "bca.simple",
                   level = .95, ci = TRUE,
                   standardized = FALSE)
```

```
##                 lhs op                          rhs            label
## 1            read_c  ~                       math_c               a1
## 2           science  ~                       read_c               b1
## 3            read_c  ~                      write_c               a2
## 4            read_c  ~               math_c.write_c               a3
## 5           science  ~                       math_c            cdash
## 6           write_c ~1                                    write.mean
## 7           write_c ~~                      write_c        write.var
## 8            read_c ~~                       read_c                 
## 9           science ~~                      science                 
## 10           math_c ~~                       math_c                 
## 11           math_c ~~               math_c.write_c                 
## 12   math_c.write_c ~~               math_c.write_c                 
## 13           read_c ~1                                              
## 14          science ~1                                              
## 15           math_c ~1                                              
## 16   math_c.write_c ~1                                              
## 17    index.mod.med :=                        a3*b1    index.mod.med
## 18 indirect.SDbelow := a1*b1+a3*-sqrt(write.var)*b1 indirect.SDbelow
## 19    indirect.mean :=       a1*b1+a3*write.mean*b1    indirect.mean
## 20 indirect.SDabove :=  a1*b1+a3*sqrt(write.var)*b1 indirect.SDabove
##         est    se       z pvalue ci.lower ci.upper
## 1     0.507 0.074   6.816  0.000    0.358    0.650
## 2     0.365 0.075   4.888  0.000    0.225    0.517
## 3     0.340 0.070   4.833  0.000    0.208    0.475
## 4     0.004 0.006   0.736  0.462   -0.008    0.017
## 5     0.402 0.084   4.793  0.000    0.243    0.566
## 6     0.000 0.625   0.000  1.000   -1.254    1.195
## 7    89.394 6.836  13.076  0.000   76.174  103.413
## 8    52.636 4.873  10.802  0.000   45.073   65.138
## 9    50.894 5.161   9.861  0.000   41.118   62.001
## 10   87.329 0.000      NA     NA   87.329   87.329
## 11   91.757 0.000      NA     NA   91.757   91.757
## 12 6358.530 0.000      NA     NA 6358.530 6358.530
## 13   -0.245 0.617  -0.397  0.691   -1.478    0.948
## 14   51.850 0.496 104.621  0.000   50.851   52.789
## 15    0.000 0.000      NA     NA    0.000    0.000
## 16   54.555 0.000      NA     NA   54.555   54.555
## 17    0.002 0.002   0.734  0.463   -0.003    0.006
## 18    0.170 0.052   3.290  0.001    0.089    0.297
## 19    0.185 0.042   4.384  0.000    0.119    0.295
## 20    0.201 0.042   4.739  0.000    0.129    0.304
```

> ### Right now I'm too lazy to explore assumptions or diagnostics or interpretations. Of course, whether I "ran these analyses right" depends on the data meeting assumptions, but my goal here was simpler than that: replicate Hayes's macro outputs. Win.

> ### If you're interested in assumptions or diagnostics or interpretations of these models--I think you should be--then I recommend checking out Hayes's [book](http://www.guilford.com/books/Introduction-to-Mediation-Moderation-and-Conditional-Process-Analysis/Andrew-Hayes/9781609182304). Also, if you haven't guessed, these models are SEM models; any good material on SEM = good material for learning when and how to run and interpret these kinds of models. The lavaan people recommend Alex Beaujean's book: [Latent Variable Modeling using R: A Step-By-Step Guide](http://blogs.baylor.edu/rlatentvariable/).

> ### I linked Hayes's template for his 76 models above, but [here](http://afhayes.com/public/templates.pdf) it is again. With some time and some gumption, you should be able to apply the code in this post to run every or nearly every the model in that PDF. You can find even more models [here](http://offbeat.group.shef.ac.uk/FIO/mplusmedmod.htm). I also suggest visting [lavaan's webpage](http://lavaan.ugent.be/) for really simple and pretty tutorials on how to use their package.

> ### Happy R

### Papers I cited (in order)

1. Baron, R. M., & Kenny, D. A. (1986). The moderator-mediator variable distinction in social psychological research: conceptual, strategic, and statistical considerations. Journal of Personality and Social Psychology, 51(6), 1173-1182.
2. Aiken, L. S., West, S. G., & Reno, R. R. (1991). Multiple regression: Testing and interpreting interactions. New York, NY: Sage.
3. Preacher, K. J., & Hayes, A. F. (2008). Asymptotic and resampling strategies for assessing and comparing indirect effects in multiple mediator models. Behavior Research Methods, 40(3), 879-891.

> ### Update: I forgot Hayes's _Index of Moderated Mediation_. What's that?

> "The heart of the test is a quantification of the association between an indirect effect and a moderator—an "index of moderated mediation"—followed by an inference as to whether this index is different from zero. I show that if the outcome of this test results in a claim that an indirect effect is moderated, this means that any two conditional indirect effects estimated at different values of the moderator are significantly different from each other."

> ### I ran the model again and saved page space by only pasting the index below. 




