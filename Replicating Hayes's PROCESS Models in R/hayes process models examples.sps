﻿* Encoding: UTF-8.
GET
  FILE='C:\Users\nickmm\Desktop\mediation_data.sav'.
DATASET NAME DataSet2 WINDOW=FRONT.

# moderation with continuous moderator
process vars = socst read math
/ x = socst
/m =math
/ y = read
/ model = 1
/ center = 1
/ boot = 1000.

# classical mediation
process vars = science read math
/ x = math
/m =read
/ y = science
/ model = 4
/ center = 1
/ total = 1
/ effsize = 1
/ boot = 1000.

# moderated mediation
process vars = science read math write
/ x = math
/m =read
/ w = write
/ y = science
/ model = 7
/ center = 1
/ total = 1
/ boot = 1000.