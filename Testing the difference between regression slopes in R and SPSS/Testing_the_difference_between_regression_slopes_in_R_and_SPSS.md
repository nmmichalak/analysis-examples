# Testing the difference between regression slopes in R and SPSS
Nick Michalak  
12/19/2016  

# testing claim from published statistics


```r
# coefficients and their standard errors
b1 <- 0.48
seb1 <- 0.084
b2 <- 0.059
seb2 <- 0.086
t.critical <- qt(p = 1 - .05/2, df = 46 - 3)

b1.lwr.95 <- b1 - seb1*t.critical
b1.upr.95 <- b1 + seb1*t.critical
b2.lwr.95 <- b2 - seb2*t.critical
b2.upr.95 <- b2 + seb2*t.critical

data.frame(b = c(b1,b2),
           lwr.95 = c(b1.lwr.95,b2.lwr.95),
           upr.95 = c(b1.upr.95,b2.upr.95))
```

```
##       b     lwr.95    upr.95
## 1 0.480  0.3105979 0.6494021
## 2 0.059 -0.1144355 0.2324355
```

```r
# Z
Z = (b1 - b2)/sqrt(seb1^2 + seb2^2)
Z
```

```
## [1] 3.502016
```

```r
# two-tailed p-value
2*pnorm(q = -Z, mean = 0, sd = 1)
```

```
## [1] 0.000461752
```

# example data


```r
# foreign package has functions for reading special file formats
library(foreign)

# path to the data
example.path <- "~/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/mediation_data.sav"

# read data
example.data <- read.spss(file = example.path,
                     use.value.labels = TRUE,
                     to.data.frame = TRUE,
                     use.missings = TRUE)
```

```
## Warning in read.spss(file = example.path, use.value.labels = TRUE,
## to.data.frame = TRUE, : ~/Desktop/analysis-examples/Replicating Hayes's
## PROCESS Models in R/mediation_data.sav: Unrecognized record type 7, subtype
## 18 encountered in system file
```

# describe data


```r
# psych package has a lot of statistical tools for the social sciences
library(psych)

# dplyr makes manipulating data easier with intuitive syntax and functions
library(dplyr)
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
# center variables
example.data[,c("math.c","science.c","socst.c")] <- lapply(X = example.data[,c("math","science","socst")],
                                                           FUN = function(x) {
                                                             as.numeric(x = scale(x = x,
                                                                                  center = TRUE,
                                                                                  scale = FALSE))
                                                           })

# describe data
describe(x = example.data[,c("math.c","science.c","socst.c","read")])
```

```
##           vars   n  mean    sd median trimmed   mad    min   max range
## math.c       1 200  0.00  9.37  -0.65   -0.41 10.38 -19.65 22.35    42
## science.c    2 200  0.00  9.90   1.15    0.17 11.86 -25.85 22.15    48
## socst.c      3 200  0.00 10.74  -0.41    0.58 13.34 -26.41 18.59    45
## read         4 200 52.23 10.25  50.00   52.03 10.38  28.00 76.00    48
##            skew kurtosis   se
## math.c     0.28    -0.69 0.66
## science.c -0.19    -0.60 0.70
## socst.c   -0.38    -0.57 0.76
## read       0.19    -0.66 0.72
```

```r
# correlation plots (hint: strong correlations = multicolinearity is coming)
example.data[,c("math.c","science.c","socst.c","read")] %>% pairs.panels(., smooth = TRUE, pch = ".")
```

![](Testing_the_difference_between_regression_slopes_in_R_and_SPSS_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

```r
# function for creating SD categories of variables
sd.groups <- function(x){
  stdev = sd(x)
  avg = mean(x)
  sd.grp = factor(x = ifelse(x < (avg - stdev), "-1 SD",
                      ifelse(x > (avg + stdev), "+1 SD",
                      ifelse(is.na(x) == FALSE, "Mean", NA))), levels = c("-1 SD","Mean","+1 SD"), ordered = TRUE)
  
  return(sd.grp)
}

# create factor variables at ±1 SD
example.data[,c("math.grp","science.grp","socst.grp")] <- lapply(X = example.data[,c("math.c","science.c","socst.c")],
                                                           FUN = function(x) sd.groups(x))

# check frequencies
sapply(X = example.data[,c("math.grp","science.grp","socst.grp")], FUN = table)
```

```
##       math.grp science.grp socst.grp
## -1 SD       42          31        42
## Mean       125         136       126
## +1 SD       33          33        32
```

```r
sapply(X = example.data[,c("math.grp","science.grp","socst.grp")], FUN = function(x) prop.table(x = table(x)))
```

```
##       math.grp science.grp socst.grp
## -1 SD    0.210       0.155      0.21
## Mean     0.625       0.680      0.63
## +1 SD    0.165       0.165      0.16
```

# graph 2D moderation


```r
# ggplot2 is the best graphing package
library(ggplot2)
```

```
## Warning: package 'ggplot2' was built under R version 3.3.2
```

```
## 
## Attaching package: 'ggplot2'
```

```
## The following objects are masked from 'package:psych':
## 
##     %+%, alpha
```

```r
# artyfarty is a package full of pretty ggplot2 themes
library(artyfarty)

# math as the moderator
ggplot(data = example.data, aes(x = socst.c, y = read, color = math.grp)) +
  geom_smooth(method = "lm") +
  geom_point() +
  labs(x = "socst (centered)", y = "read", color = "±1 SD math") +
  guides(bg = FALSE) +
  theme_scientific() +
  scale_color_manual(values = pal("five38"))
```

![](Testing_the_difference_between_regression_slopes_in_R_and_SPSS_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

```r
# science as the moderator
ggplot(data = example.data, aes(x = socst.c, y = read, color = science.grp)) +
  geom_smooth(method = "lm") +
  geom_point() +
  labs(x = "socst (centered)", y = "read", color = "±1 SD science") +
  guides(bg = FALSE) +
  theme_scientific() +
  scale_color_manual(values = pal("five38"))
```

![](Testing_the_difference_between_regression_slopes_in_R_and_SPSS_files/figure-html/unnamed-chunk-4-2.png)<!-- -->

# test difference between dependent correlations


```r
# print correlations and CIS
print(corr.test(example.data[,c("math.c","science.c","socst.c","read")]), short = FALSE)
```

```
## Call:corr.test(x = example.data[, c("math.c", "science.c", "socst.c", 
##     "read")])
## Correlation matrix 
##           math.c science.c socst.c read
## math.c      1.00      0.63    0.54 0.66
## science.c   0.63      1.00    0.47 0.63
## socst.c     0.54      0.47    1.00 0.62
## read        0.66      0.63    0.62 1.00
## Sample Size 
## [1] 200
## Probability values (Entries above the diagonal are adjusted for multiple tests.) 
##           math.c science.c socst.c read
## math.c         0         0       0    0
## science.c      0         0       0    0
## socst.c        0         0       0    0
## read           0         0       0    0
## 
##  To see confidence intervals of the correlations, print with the short=FALSE option
## 
##  Confidence intervals based upon normal theory.  To get bootstrapped values, try cor.ci
##             lower    r upper p
## mth.c-scnc.  0.54 0.63  0.71 0
## mth.c-scst.  0.44 0.54  0.64 0
## mth.c-read   0.58 0.66  0.73 0
## scnc.-scst.  0.35 0.47  0.57 0
## scnc.-read   0.54 0.63  0.71 0
## scst.-read   0.53 0.62  0.70 0
```

```r
# test difference between r of math-read and r of science-read
r.test(n = nrow(example.data),
       r12 = .66,
       r34 = 0.63,
       r23 = 0.63,
       pooled = FALSE,
       twotailed = TRUE)
```

```
## Correlation tests 
## Call:[1] "r.test(n =  200 ,  r12 =  0.66 ,  r23 =  0.63 ,  r13 =  0.63 )"
## Test of difference between two correlated  correlations 
##  t value 0.69    with probability < 0.49
```

# model 2-way and 3-way interaction models (spoiler: there isn't one)


```r
# two way interaction between socst and math (both centered)
two.way.math.mod <- lm(read ~ socst.c*math.c, data = example.data)

# two way interaction between socst and science (both centered)
two.way.sci.mod <- lm(read ~ socst.c*science.c, data = example.data)

# two way interaction between socst and science (both centered)
three.way.mod <- lm(read ~ socst.c*math.c*science.c, data = example.data)

# run them all together
sapply(X = list(two.way.math.mod,two.way.sci.mod,three.way.mod), FUN = function(x){
  summary(x)
},
simplify = FALSE,
USE.NAMES = TRUE)
```

```
## [[1]]
## 
## Call:
## lm(formula = read ~ socst.c * math.c, data = example.data)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -18.6071  -4.9228  -0.7195   4.5912  21.8592 
## 
## Coefficients:
##                 Estimate Std. Error t value Pr(>|t|)    
## (Intercept)    51.615327   0.568685  90.763  < 2e-16 ***
## socst.c         0.373829   0.055546   6.730 1.82e-10 ***
## math.c          0.480654   0.063701   7.545 1.65e-12 ***
## socst.c:math.c  0.011281   0.005229   2.157   0.0322 *  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.96 on 196 degrees of freedom
## Multiple R-squared:  0.5461,	Adjusted R-squared:  0.5392 
## F-statistic: 78.61 on 3 and 196 DF,  p-value: < 2.2e-16
## 
## 
## [[2]]
## 
## Call:
## lm(formula = read ~ socst.c * science.c, data = example.data)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -20.1764  -5.1030   0.2893   4.7523  17.5854 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)       51.812080   0.541528  95.678  < 2e-16 ***
## socst.c            0.391997   0.052277   7.499 2.18e-12 ***
## science.c          0.469635   0.057374   8.185 3.40e-14 ***
## socst.c:science.c  0.008496   0.004516   1.881   0.0614 .  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.984 on 196 degrees of freedom
## Multiple R-squared:  0.543,	Adjusted R-squared:  0.536 
## F-statistic: 77.62 on 3 and 196 DF,  p-value: < 2.2e-16
## 
## 
## [[3]]
## 
## Call:
## lm(formula = read ~ socst.c * math.c * science.c, data = example.data)
## 
## Residuals:
##      Min       1Q   Median       3Q      Max 
## -17.4265  -4.5985  -0.3497   4.5646  15.9406 
## 
## Coefficients:
##                            Estimate Std. Error t value Pr(>|t|)    
## (Intercept)              51.9382823  0.5777152  89.903  < 2e-16 ***
## socst.c                   0.3534500  0.0635594   5.561 8.90e-08 ***
## math.c                    0.3544337  0.0722738   4.904 1.99e-06 ***
## science.c                 0.3067530  0.0684304   4.483 1.27e-05 ***
## socst.c:math.c            0.0187256  0.0071073   2.635  0.00911 ** 
## socst.c:science.c         0.0015239  0.0065141   0.234  0.81529    
## math.c:science.c         -0.0137123  0.0069196  -1.982  0.04895 *  
## socst.c:math.c:science.c -0.0005424  0.0005695  -0.952  0.34214    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.54 on 192 degrees of freedom
## Multiple R-squared:  0.6074,	Adjusted R-squared:  0.5931 
## F-statistic: 42.44 on 7 and 192 DF,  p-value: < 2.2e-16
```

# Looks like the 2-way interaction for socst x math is greater than the 2-way for math x science, even though they're both significant.
# Let's test the difference


```r
# multcomp is a package for multiple comparisons
library(multcomp)
```

```
## Loading required package: mvtnorm
```

```
## Loading required package: survival
```

```
## Loading required package: TH.data
```

```
## Loading required package: MASS
```

```
## 
## Attaching package: 'MASS'
```

```
## The following object is masked from 'package:dplyr':
## 
##     select
```

```
## 
## Attaching package: 'TH.data'
```

```
## The following object is masked from 'package:MASS':
## 
##     geyser
```

```r
# contrast for testing socst.c:math.c (-1) against math.c:science.c (1)
# all other coefficients get zero weights
socstVscience <- rbind("socstVscience" = c(0,0,0,0,-1,0,1,0))
socstVscience
```

```
##               [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8]
## socstVscience    0    0    0    0   -1    0    1    0
```

```r
# looks like they are significantly different
summary(object = glht(model = three.way.mod,
                      linfct = socstVscience),
        test = adjusted("none"))
```

```
## 
## 	 Simultaneous Tests for General Linear Hypotheses
## 
## Fit: lm(formula = read ~ socst.c * math.c * science.c, data = example.data)
## 
## Linear Hypotheses:
##                    Estimate Std. Error t value Pr(>|t|)   
## socstVscience == 0 -0.03244    0.01068  -3.036  0.00273 **
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## (Adjusted p values reported -- none method)
```

# Looks like the main effect for math is greater than the main effect for science, even though they're both significant.
# Test it!


```r
# contrast for testing math.c (-1) against science.c (1)
# all other coefficients get zero weights
mathVscience <- rbind("socstVscience" = c(0,0,-1,1,0,0,0,0))
mathVscience
```

```
##               [,1] [,2] [,3] [,4] [,5] [,6] [,7] [,8]
## socstVscience    0    0   -1    1    0    0    0    0
```

```r
# looks like they are NOT significantly different
summary(object = glht(model = three.way.mod,
                      linfct = mathVscience),
        test = adjusted("none"))
```

```
## 
## 	 Simultaneous Tests for General Linear Hypotheses
## 
## Fit: lm(formula = read ~ socst.c * math.c * science.c, data = example.data)
## 
## Linear Hypotheses:
##                    Estimate Std. Error t value Pr(>|t|)
## socstVscience == 0 -0.04768    0.11871  -0.402    0.688
## (Adjusted p values reported -- none method)
```

