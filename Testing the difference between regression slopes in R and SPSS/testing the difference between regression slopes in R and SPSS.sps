﻿* get the data

get file = "/Users/nicholasmmichalak/Desktop/analysis-examples/Replicating Hayes's PROCESS Models in R/mediation_data.sav".

* create average columns for variables

aggregate
 /outfile=* mode=addvariables
 /break=
 /math.mean=mean(math) 
 /science.mean=mean(science) 
 /socst.mean=mean(socst).

* center variables and create interaction terms

compute math.c = math - math.mean.
compute science.c = science - science.mean.
compute socst.c = socst - socst.mean.
compute math.science.c = math.c * science.c.
compute math.socst.c = math.c * socst.c.
compute science.socst.c = science.c * socst.c.
compute math.science.socst.c = math.c * science.c * socst.c.

* descriptives

descriptives variables=math.c science.c socst.c
  /statistics=all.

* correlation of centered variables, their interactions, and the DV

correlations
  /variables=read socst.c math.c science.c math.science.c math.socst.c science.socst.c math.science.socst.c
  /print=twotail.

* test full model and two regression coefficient contrasts

glm read with socst.c math.c science.c math.science.c math.socst.c science.socst.c math.science.socst.c
/print=parameter
/design = socst.c math.c science.c math.science.c math.socst.c science.socst.c math.science.socst.c
/lmatrix = 'math.socst.c = math.science.c' math.socst.c -1 math.science.c 1
/lmatrix = 'math.c = science.c' math.c -1 science.c 1.
