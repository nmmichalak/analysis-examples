# Bar and Line Graphs in R and SPSS
Nick Michalak  
June 25, 2016  


```r
if(!("Hmisc" %in% rownames(installed.packages())))
install.packages("Hmisc")
library(Hmisc)

exp4 <- spss.get(file = "~/Desktop/analysis-examples/Bar and Line Graphs in R and SPSS/Experiment 4.sav", lowernames = TRUE, use.value.labels = TRUE, to.data.frame = TRUE)
```


```r
if(!("gmodels" %in% rownames(installed.packages())))
install.packages("gmodels")
library(gmodels)

if(!("car" %in% rownames(installed.packages())))
install.packages("car")
library(car)

tapply(X = exp4$pro.stickers.donated, INDEX = list(exp4$group, exp4$gender), length)
```

```
##         girl boy
## money     25  17
## buttons   21  21
## candies   22  19
```

```r
fit.contrast(model = lm(formula = pro.stickers.donated ~ group, data = exp4), varname = "group", coeff = c(-1, 1, 0), conf.int = .95, df = T)
```

```
##                     Estimate Std. Error  t value     Pr(>|t|)  DF
## group c=( -1 1 0 ) 0.2615079 0.04836314 5.407175 3.235014e-07 122
##                     lower CI  upper CI
## group c=( -1 1 0 ) 0.1657683 0.3572476
```

```r
Anova(lm(formula = pro.stickers.donated ~ group, data = exp4), type = "III")
```

```
## Anova Table (Type III tests)
## 
## Response: pro.stickers.donated
##             Sum Sq  Df F value    Pr(>F)    
## (Intercept) 2.1413   1  43.594 1.108e-09 ***
## group       1.5968   2  16.255 5.516e-07 ***
## Residuals   5.9925 122                      
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
Anova(lm(formula = pro.stickers.donated ~ group + age, data = exp4), type = "III")
```

```
## Anova Table (Type III tests)
## 
## Response: pro.stickers.donated
##             Sum Sq  Df F value    Pr(>F)    
## (Intercept) 0.8695   1  18.398 3.628e-05 ***
## group       1.6127   2  17.061 2.970e-07 ***
## age         0.2738   1   5.794   0.01759 *  
## Residuals   5.7187 121                      
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
fit.contrast(model = lm(pro.stickers.donated ~ gender, data = exp4), varname = "gender", coeff = c(1, -1), conf.int = .95, df = T)
```

```
##                      Estimate Std. Error   t value   Pr(>|t|)  DF
## gender c=( 1 -1 ) -0.08166065 0.04399612 -1.856087 0.06583428 123
##                     lower CI    upper CI
## gender c=( 1 -1 ) -0.1687483 0.005426976
```

```r
cor.test(~exp4$age +exp4$pro.stickers.donated)
```

```
## 
## 	Pearson's product-moment correlation
## 
## data:  exp4$age and exp4$pro.stickers.donated
## t = -2.0805, df = 123, p-value = 0.03955
## alternative hypothesis: true correlation is not equal to 0
## 95 percent confidence interval:
##  -0.348697550 -0.009066399
## sample estimates:
##        cor 
## -0.1843804
```


```r
ggplot(data = exp4, aes(x = gender, y = pro.stickers.donated, fill = group)) +
  stat_summary(fun.data = "mean_se", geom = "bar", position = position_dodge(1)) +
  stat_summary(fun.data = "mean_se", geom = "errorbar", position = position_dodge(1), width = 0.1) +
  scale_y_continuous(limits = seq(0, 1), labels = scales::percent) +
  stat_summary(aes(label = scales::percent(..y..)), fun.data = "mean_se", geom = "text", position = position_dodge(1), vjust = 8, fontface = "bold", ) +
  scale_fill_brewer(palette = "Set1") +
  labs(x = NULL, y = "Stickers dondated", fill = "Group") +
  theme_gray(base_size = 12, base_family = "Helvetica")
```

![](Bar_and_Line_Graphs_in_R_and_SPSS_files/figure-html/unnamed-chunk-3-1.png)<!-- -->


```r
if(!("reshape2" %in% rownames(installed.packages())))
install.packages("reshape2")
library(reshape2)

exp4_props <- melt(data = sapply(seq(0, 1, .1), function(x) by(exp4$pro.stickers.donated >= x, exp4$group, mean)))

names(exp4_props) <- c("group", "min_prop_donated", "prop_children")

exp4_props$min_prop_donated <- as.numeric(as.character(factor(exp4_props$min_prop_donated , levels = 1:11, labels = seq(0, 1, .1))))

exp4_ns <- melt(data = sapply(seq(0, 1, .1), function(x) by(exp4$pro.stickers.donated >= x, exp4$group, length)))

names(exp4_ns) <- c("group", "min_prop_donated", "count_children")

exp4_ns$min_prop_donated <- as.numeric(as.character(factor(exp4_ns$min_prop_donated , levels = 1:11, labels = seq(0, 1, .1))))

exp4_props_ns <- cbind(exp4_props,count_children = exp4_ns$count_children)

exp4_props_ns$se <- with(exp4_props_ns, sqrt((prop_children*(1 - prop_children))/count_children))

ggplot(exp4_props_ns, aes(x = min_prop_donated, y = prop_children, shape = group, group = group, ymin = prop_children - se, ymax = prop_children + se)) + geom_line() + geom_errorbar(width = 0.0125) + geom_point(size = 3) + scale_y_continuous(labels = scales::percent) + scale_x_continuous(labels = scales::percent) + labs(x = "Minimum percent of stickers donated", y = "Percentage of children", shape = "Condition") + theme_grey(base_size = 12, base_family = "Helvetica")
```

![](Bar_and_Line_Graphs_in_R_and_SPSS_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

