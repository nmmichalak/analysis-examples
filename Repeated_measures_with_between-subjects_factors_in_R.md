# Reapeated measures with between-subjects factors in R
Nick Michalak  
May 31, 2016  

## load required packages


    want_packages <- c("reshape2","car","multcomp","ggplot2","knitr")
    have_packages   <- want_packages %in% rownames(installed.packages())
    if(any(!have_packages)) install.packages(want_packages[!have_packages])
    library(reshape2)
    library(car)
    library(multcomp)

    ## Loading required package: mvtnorm

    ## Loading required package: survival

    ## Loading required package: TH.data

    library(ggplot2)
    library(knitr)

    ## Warning: package 'knitr' was built under R version 3.2.5

    options(repos = structure(c(CRAN = "http://cran.rstudio.com/")))
    render_markdown(strict = TRUE)

## data
[UCLA SPSS Repeated Measures tutorial](http://www.ats.ucla.edu/stat/spss/seminars/Repeated_Measures/)

### Exercise data examples
> The data consists of people who were randomly assigned to two different diets: low-fat and not low-fat and three different types of exercise: at rest, walking leisurely and running. Their pulse rate was measured at three different time points during their assigned exercise: at 1 minute, 15 minutes and 30 minutes.


    points <- c(1,1,1,85,85,88,
       2,1,1,90,92,93,
       3,1,1,97,97,94,
       4,1,1,80,82,83,
       5,1,1,91,92,91,
       6,1,2,83,83,84,
       7,1,2,87,88,90,
       8,1,2,92,94,95,
       9,1,2,97,99,96,
       10,1,2,100,97,100,
       11,2,1,86,86,84,
       12,2,1,93,103,104,
       13,2,1,90,92,93,
       14,2,1,95,96,100,
       15,2,1,89,96,95,
       16,2,2,84,86,89,
       17,2,2,103,109,90,
       18,2,2,92,96,101,
       19,2,2,97,98,100,
       20,2,2,102,104,103,
       21,3,1,93,98,110,
       22,3,1,98,104,112,
       23,3,1,98,105,99,
       24,3,1,87,132,120,
       25,3,1,94,110,116,
       26,3,2,95,126,143,
       27,3,2,100,126,140,
       28,3,2,103,124,140,
       29,3,2,94,135,130,
       30,3,2,99,111,150
    )
    
    matr <- matrix(data = points, ncol = 6, byrow = T)
    df <- data.frame(matr)
    names(df) <- c("id", "exertype", "diet", "time1", "time2", "time3")
    df[,c("id","exertype","diet")] <- lapply(df[,c("id","exertype","diet")],factor)
    
    head(df)

    ##   id exertype diet time1 time2 time3
    ## 1  1        1    1    85    85    88
    ## 2  2        1    1    90    92    93
    ## 3  3        1    1    97    97    94
    ## 4  4        1    1    80    82    83
    ## 5  5        1    1    91    92    91
    ## 6  6        1    2    83    83    84

    setwd("/Users/nmmichalak/Desktop/analysis-examples/")
    write.csv(x = df, file = "exercise_diet_example.csv",row.names = F)

## multivariate approach, going straight to testing contrasts

### linear and quadratic contrasts, only time factor (within, 3 levels)

    summary(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% cbind(c(-1,0,1),
                                                              c(1, -2, 1)) ~ 1
        
      )
    )

    ## Response Y1 :
    ## 
    ## Call:
    ## lm(formula = Y1 ~ 1)
    ## 
    ## Residuals:
    ##    Min     1Q Median     3Q    Max 
    ## -24.30 -10.30  -8.30   4.95  39.70 
    ## 
    ## Coefficients:
    ##             Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)    11.30       3.02   3.742 0.000803 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 16.54 on 29 degrees of freedom
    ## 
    ## 
    ## Response Y2 :
    ## 
    ## Call:
    ## lm(formula = Y2 ~ 1)
    ## 
    ## Residuals:
    ##    Min     1Q Median     3Q    Max 
    ## -51.50  -3.25   4.00   6.50  32.50 
    ## 
    ## Coefficients:
    ##             Estimate Std. Error t value Pr(>|t|)  
    ## (Intercept)   -5.500      2.787  -1.973    0.058 .
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 15.26 on 29 degrees of freedom

### diet factor (between, 2 levels), ignoring time


      Anova(
        lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(1/3, 1/3, 1/3) ~ diet,
         data = df,
         contrasts = list(diet = c(-1,1))
         
      ),type = "II")

    ## Anova Table (Type II tests)
    ## 
    ## Response: as.matrix(df[, c("time1", "time2", "time3")]) %*% c(1/3, 1/3, 
    ## Response:     1/3)
    ##           Sum Sq Df F value  Pr(>F)  
    ## diet       420.6  1  3.1471 0.08694 .
    ## Residuals 3742.3 28                  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

      summary(
        glht(
          lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(1/3, 1/3, 1/3) ~ diet, data = df),
          linfct = mcp(diet = c(-1,1)),
          alternative = "two.sided"),
        test = adjusted("none"))

    ## 
    ## 	 Simultaneous Tests for General Linear Hypotheses
    ## 
    ## Multiple Comparisons of Means: User-defined Contrasts
    ## 
    ## 
    ## Fit: lm(formula = as.matrix(df[, c("time1", "time2", "time3")]) %*% 
    ##     c(1/3, 1/3, 1/3) ~ diet, data = df)
    ## 
    ## Linear Hypotheses:
    ##        Estimate Std. Error t value Pr(>|t|)  
    ## 1 == 0    7.489      4.221   1.774   0.0869 .
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## (Adjusted p values reported -- none method)

### exertype factor (between, 3 levels), ignoring time


    Anova(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(1/3, 1/3, 1/3) ~ exertype,
         data = df,
         contrasts = list(exertype = c(1, 0, -1))
         
      ), type = "II")

    ## Anova Table (Type II tests)
    ## 
    ## Response: as.matrix(df[, c("time1", "time2", "time3")]) %*% c(1/3, 1/3, 
    ## Response:     1/3)
    ##           Sum Sq Df F value   Pr(>F)    
    ## exertype  2775.4  2  27.001 3.62e-07 ***
    ## Residuals 1387.6 27                     
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

    excertypeCompare <- rbind("1v3" = c(1, 0, -1),
                              "1v2" = c(1, -1, 0),
                              "2v3" = c(0, 1, -1))
    summary(
      glht(
        lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(1/3, 1/3, 1/3) ~ exertype,
           data = df),
        linfct = mcp(exertype = excertypeCompare),
        alternative = "two.sided"),
      test = adjusted("none"))

    ## 
    ## 	 Simultaneous Tests for General Linear Hypotheses
    ## 
    ## Multiple Comparisons of Means: User-defined Contrasts
    ## 
    ## 
    ## Fit: lm(formula = as.matrix(df[, c("time1", "time2", "time3")]) %*% 
    ##     c(1/3, 1/3, 1/3) ~ exertype, data = df)
    ## 
    ## Linear Hypotheses:
    ##          Estimate Std. Error t value Pr(>|t|)    
    ## 1v3 == 0  -22.233      3.206  -6.935 1.88e-07 ***
    ## 1v2 == 0   -4.367      3.206  -1.362    0.184    
    ## 2v3 == 0  -17.867      3.206  -5.573 6.57e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## (Adjusted p values reported -- none method)

### now diet factor (between, 2 levels) x time factor (within, 3 levels) computing only the linear contrast for time


    Anova(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ diet,
         data = df, contrasts = list(diet = contr.sum(2))
      ), type = "II"
    )

    ## Anova Table (Type II tests)
    ## 
    ## Response: as.matrix(df[, c("time1", "time2", "time3")]) %*% c(-1, 0, 1)
    ##           Sum Sq Df F value Pr(>F)
    ## diet       381.6  1  1.4148 0.2442
    ## Residuals 7552.7 28

    summary(
      glht(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ diet,
         data = df),
      linfct = mcp(diet = c(-1,1))),
      test = adjusted("none")
    )

    ## 
    ## 	 Simultaneous Tests for General Linear Hypotheses
    ## 
    ## Multiple Comparisons of Means: User-defined Contrasts
    ## 
    ## 
    ## Fit: lm(formula = as.matrix(df[, c("time1", "time2", "time3")]) %*% 
    ##     c(-1, 0, 1) ~ diet, data = df)
    ## 
    ## Linear Hypotheses:
    ##        Estimate Std. Error t value Pr(>|t|)
    ## 1 == 0    7.133      5.997   1.189    0.244
    ## (Adjusted p values reported -- none method)

### now exertype factor (between, 3 levels) x time factor (within, 3 levels) computing only the linear contrast for time


    Anova(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ exertype,
         data = df, contrasts = list(exertype = c(1, 0, -1))),
      type = "II")

    ## Anova Table (Type II tests)
    ## 
    ## Response: as.matrix(df[, c("time1", "time2", "time3")]) %*% c(-1, 0, 1)
    ##           Sum Sq Df F value    Pr(>F)    
    ## exertype  5202.2  2  25.705 5.615e-07 ***
    ## Residuals 2732.1 27                      
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

    summary(
      glht(
          lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ exertype,data = df),
          linfct = mcp(exertype = excertypeCompare),
          alternative = "two.sided"),
      test = adjusted("none"))

    ## 
    ## 	 Simultaneous Tests for General Linear Hypotheses
    ## 
    ## Multiple Comparisons of Means: User-defined Contrasts
    ## 
    ## 
    ## Fit: lm(formula = as.matrix(df[, c("time1", "time2", "time3")]) %*% 
    ##     c(-1, 0, 1) ~ exertype, data = df)
    ## 
    ## Linear Hypotheses:
    ##          Estimate Std. Error t value Pr(>|t|)    
    ## 1v3 == 0  -28.700      4.499  -6.380 7.83e-07 ***
    ## 1v2 == 0   -1.600      4.499  -0.356    0.725    
    ## 2v3 == 0  -27.100      4.499  -6.024 1.99e-06 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## (Adjusted p values reported -- none method)

### now exertype factor (between, 3 levels) x diet factor (between), 2 levels time factor (within, 3 levels) computing only the linear contrast for time


    Anova(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ exertype*diet,
         data = df, contrasts = list(exertype = c(1, 0, -1),
                                     diet = contr.sum(2))
      ),type = "II")

    ## Anova Table (Type II tests)
    ## 
    ## Response: as.matrix(df[, c("time1", "time2", "time3")]) %*% c(-1, 0, 1)
    ##               Sum Sq Df F value    Pr(>F)    
    ## exertype      5202.2  2 54.8948  1.11e-09 ***
    ## diet           381.6  1  8.0542 0.0090888 ** 
    ## exertype:diet 1213.3  2 12.8027 0.0001645 ***
    ## Residuals     1137.2 24                      
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

    df$group <- with(df, interaction(exertype, diet))
    levels(df$group)

    ## [1] "1.1" "2.1" "3.1" "1.2" "2.2" "3.2"

    ThreeWayCompares <- rbind("exer1Vexer3" = c(1, 0, -1, 1, 0, -1),
                              "diet1Vdiet2" = c(1, -1, 1, -1, 1, -1),
                              "interaction" = c(1, 0, -1, -1, 0, 1))
    
    summary(
      glht(
      lm(as.matrix(df[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ group,
       data = df), linfct = mcp(group = ThreeWayCompares),
      alternative = "two.sided"),
      test = adjusted("none")
    )

    ## 
    ## 	 Simultaneous Tests for General Linear Hypotheses
    ## 
    ## Multiple Comparisons of Means: User-defined Contrasts
    ## 
    ## 
    ## Fit: lm(formula = as.matrix(df[, c("time1", "time2", "time3")]) %*% 
    ##     c(-1, 0, 1) ~ group, data = df)
    ## 
    ## Linear Hypotheses:
    ##                  Estimate Std. Error t value Pr(>|t|)    
    ## exer1Vexer3 == 0  -57.400      6.157  -9.323  1.9e-09 ***
    ## diet1Vdiet2 == 0  -28.600      7.541  -3.793 0.000888 ***
    ## interaction == 0   25.000      6.157   4.061 0.000452 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## (Adjusted p values reported -- none method)

### interaction plot


    ggplot(data = melt(data = df,
                       id.vars = c("id","exertype","diet"),
                       measure.vars = c("time1","time2","time3"),
                       variable.name = "time", value.name = "pulseRate"),
           aes(x = time, y = pulseRate, color = exertype, group = exertype)
    ) + stat_summary(fun.data = "mean_se",geom = "line") + stat_summary(fun.data = "mean_se",geom = "point") + facet_wrap(~diet) + scale_color_brewer(palette = "Set1") + labs(x = "Time", y = "Pulse Rate (Beats per min.)", color = "Exercise Type") + stat_summary(aes(label=..y..), fun.y = mean, geom = "text", size = 5, color = "black", vjust = -1, hjust = 1.25)

![](Repeated_measures_with_between-subjects_factors_in_R_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

## mixed-model approach (computing only the linear contrast for time)


    fit <- lm(cbind(time1, time2, time3) ~ exertype*diet,
              data = df,
              contrasts = list(exertype = c(1, 0, -1),
                               diet = contr.sum(2)))
    idata <- data.frame(time = gl(n = 3,
                                  k = 1,
                                  labels = c("time1","time2","time3")))
    
    ANOVA <- Anova(fit,
                   idata = idata,
                   idesign = ~ time,
                   type = "II")
    summary(ANOVA,
            multivariate = TRUE,
            univariate = TRUE)

    ## Warning in summary.Anova.mlm(ANOVA, multivariate = TRUE, univariate =
    ## TRUE): HF eps > 1 treated as 1

    ## 
    ## Type II Repeated Measures MANOVA Tests:
    ## 
    ## ------------------------------------------
    ##  
    ## Term: (Intercept) 
    ## 
    ##  Response transformation matrix:
    ##       (Intercept)
    ## time1           1
    ## time2           1
    ## time3           1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##             (Intercept)
    ## (Intercept)     2683824
    ## 
    ## Sum of squares and products for error:
    ##             (Intercept)
    ## (Intercept)      6255.6
    ## 
    ## Multivariate Tests: (Intercept)
    ##                  Df test stat approx F num Df den Df     Pr(>F)    
    ## Pillai            1    0.9977 10296.66      1     24 < 2.22e-16 ***
    ## Wilks             1    0.0023 10296.66      1     24 < 2.22e-16 ***
    ## Hotelling-Lawley  1  429.0275 10296.66      1     24 < 2.22e-16 ***
    ## Roy               1  429.0275 10296.66      1     24 < 2.22e-16 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: exertype 
    ## 
    ##  Response transformation matrix:
    ##       (Intercept)
    ## time1           1
    ## time2           1
    ## time3           1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##             (Intercept)
    ## (Intercept)     24978.2
    ## 
    ## Sum of squares and products for error:
    ##             (Intercept)
    ## (Intercept)      6255.6
    ## 
    ## Multivariate Tests: exertype
    ##                  Df test stat approx F num Df den Df     Pr(>F)    
    ## Pillai            2  0.799717 47.91521      2     24 4.1661e-09 ***
    ## Wilks             2  0.200283 47.91521      2     24 4.1661e-09 ***
    ## Hotelling-Lawley  2  3.992934 47.91521      2     24 4.1661e-09 ***
    ## Roy               2  3.992934 47.91521      2     24 4.1661e-09 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: diet 
    ## 
    ##  Response transformation matrix:
    ##       (Intercept)
    ## time1           1
    ## time2           1
    ## time3           1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##             (Intercept)
    ## (Intercept)    3785.633
    ## 
    ## Sum of squares and products for error:
    ##             (Intercept)
    ## (Intercept)      6255.6
    ## 
    ## Multivariate Tests: diet
    ##                  Df test stat approx F num Df den Df     Pr(>F)    
    ## Pillai            1 0.3770088 14.52382      1     24 0.00084826 ***
    ## Wilks             1 0.6229912 14.52382      1     24 0.00084826 ***
    ## Hotelling-Lawley  1 0.6051591 14.52382      1     24 0.00084826 ***
    ## Roy               1 0.6051591 14.52382      1     24 0.00084826 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: exertype:diet 
    ## 
    ##  Response transformation matrix:
    ##       (Intercept)
    ## time1           1
    ## time2           1
    ## time3           1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##             (Intercept)
    ## (Intercept)    2447.267
    ## 
    ## Sum of squares and products for error:
    ##             (Intercept)
    ## (Intercept)      6255.6
    ## 
    ## Multivariate Tests: exertype:diet
    ##                  Df test stat approx F num Df den Df   Pr(>F)  
    ## Pillai            2 0.2812024 4.694546      2     24 0.019023 *
    ## Wilks             2 0.7187976 4.694546      2     24 0.019023 *
    ## Hotelling-Lawley  2 0.3912121 4.694546      2     24 0.019023 *
    ## Roy               2 0.3912121 4.694546      2     24 0.019023 *
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: time 
    ## 
    ##  Response transformation matrix:
    ##       time1 time2
    ## time1     1     0
    ## time2     0     1
    ## time3    -1    -1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##        time1 time2
    ## time1 3830.7 983.1
    ## time2  983.1 252.3
    ## 
    ## Sum of squares and products for error:
    ##        time1  time2
    ## time1 1137.2  620.6
    ## time2  620.6 1828.8
    ## 
    ## Multivariate Tests: time
    ##                  Df test stat approx F num Df den Df     Pr(>F)    
    ## Pillai            1  0.781820 41.20886      2     23 2.4909e-08 ***
    ## Wilks             1  0.218180 41.20886      2     23 2.4909e-08 ***
    ## Hotelling-Lawley  1  3.583379 41.20886      2     23 2.4909e-08 ***
    ## Roy               1  3.583379 41.20886      2     23 2.4909e-08 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: exertype:time 
    ## 
    ##  Response transformation matrix:
    ##       time1 time2
    ## time1     1     0
    ## time2     0     1
    ## time3    -1    -1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##        time1  time2
    ## time1 5202.2 1664.4
    ## time2 1664.4  547.2
    ## 
    ## Sum of squares and products for error:
    ##        time1  time2
    ## time1 1137.2  620.6
    ## time2  620.6 1828.8
    ## 
    ## Multivariate Tests: exertype:time
    ##                  Df test stat approx F num Df den Df     Pr(>F)    
    ## Pillai            2  0.835574  8.61101      4     48 2.5376e-05 ***
    ## Wilks             2  0.172191 16.21356      4     46 2.3669e-08 ***
    ## Hotelling-Lawley  2  4.762400 26.19320      4     44 3.7823e-11 ***
    ## Roy               2  4.752912 57.03495      2     24 7.6093e-10 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: diet:time 
    ## 
    ##  Response transformation matrix:
    ##       time1 time2
    ## time1     1     0
    ## time2     0     1
    ## time3    -1    -1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##          time1 time2
    ## time1 381.6333 224.7
    ## time2 224.7000 132.3
    ## 
    ## Sum of squares and products for error:
    ##        time1  time2
    ## time1 1137.2  620.6
    ## time2  620.6 1828.8
    ## 
    ## Multivariate Tests: diet:time
    ##                  Df test stat approx F num Df den Df   Pr(>F)  
    ## Pillai            1 0.2515335  3.86475      2     23 0.035726 *
    ## Wilks             1 0.7484665  3.86475      2     23 0.035726 *
    ## Hotelling-Lawley  1 0.3360652  3.86475      2     23 0.035726 *
    ## Roy               1 0.3360652  3.86475      2     23 0.035726 *
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## ------------------------------------------
    ##  
    ## Term: exertype:diet:time 
    ## 
    ##  Response transformation matrix:
    ##       time1 time2
    ## time1     1     0
    ## time2     0     1
    ## time3    -1    -1
    ## 
    ## Sum of squares and products for the hypothesis:
    ##          time1 time2
    ## time1 1213.267 711.2
    ## time2  711.200 418.4
    ## 
    ## Sum of squares and products for error:
    ##        time1  time2
    ## time1 1137.2  620.6
    ## time2  620.6 1828.8
    ## 
    ## Multivariate Tests: exertype:diet:time
    ##                  Df test stat  approx F num Df den Df     Pr(>F)    
    ## Pillai            2 0.5175006  4.188877      4     48 0.00545864 ** 
    ## Wilks             2 0.4830197  5.046854      4     46 0.00185996 ** 
    ## Hotelling-Lawley  2 1.0692319  5.880776      4     44 0.00070102 ***
    ## Roy               2 1.0682235 12.818683      2     24 0.00016324 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Univariate Type II Repeated-Measures ANOVA Assuming Sphericity
    ## 
    ##                        SS num Df Error SS den Df          F    Pr(>F)    
    ## (Intercept)        894608      1   2085.2     24 10296.6595 < 2.2e-16 ***
    ## exertype             8326      2   2085.2     24    47.9152 4.166e-09 ***
    ## diet                 1262      1   2085.2     24    14.5238 0.0008483 ***
    ## exertype:diet         816      2   2085.2     24     4.6945 0.0190230 *  
    ## time                 2067      2   1563.6     48    31.7206 1.662e-09 ***
    ## exertype:time        2723      4   1563.6     48    20.9005 4.992e-10 ***
    ## diet:time             193      2   1563.6     48     2.9597 0.0613651 .  
    ## exertype:diet:time    614      4   1563.6     48     4.7095 0.0027501 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## 
    ## Mauchly Tests for Sphericity
    ## 
    ##                    Test statistic p-value
    ## time                      0.92416 0.40372
    ## exertype:time             0.92416 0.40372
    ## diet:time                 0.92416 0.40372
    ## exertype:diet:time        0.92416 0.40372
    ## 
    ## 
    ## Greenhouse-Geisser and Huynh-Feldt Corrections
    ##  for Departure from Sphericity
    ## 
    ##                    GG eps Pr(>F[GG])    
    ## time               0.9295  5.504e-09 ***
    ## exertype:time      0.9295  1.841e-09 ***
    ## diet:time          0.9295    0.06569 .  
    ## exertype:diet:time 0.9295    0.00359 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ##                      HF eps   Pr(>F[HF])
    ## time               1.004364 1.662197e-09
    ## exertype:time      1.004364 4.991713e-10
    ## diet:time          1.004364 6.136514e-02
    ## exertype:diet:time 1.004364 2.750071e-03
