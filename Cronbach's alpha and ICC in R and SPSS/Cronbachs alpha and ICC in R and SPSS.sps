﻿* Encoding: UTF-8.

* load BFI data

GET DATA  /TYPE=TXT
  /FILE="/Users/nicholasmmichalak/Desktop/analysis-examples/Cronbach's alpha and ICC in R and SPSS/bfi.spss.csv"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS=","
  /QUALIFIER='"'
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
  A1 F4.0
  A2 F4.0
  A3 F1.0
  A4 F4.0
  A5 F1.0
  C1 F4.0
  C2 F4.0
  C3 F4.0
  C4 F1.0
  C5 F1.0
  E1 F1.0
  E2 F4.0
  E3 F4.0
  E4 F1.0
  E5 F4.0
  N1 F4.0
  N2 F4.0
  N3 F1.0
  N4 F4.0
  N5 F4.0
  O1 F1.0
  O2 F1.0
  O3 F1.0
  O4 F1.0
  O5 F1.0
  gender F1.0
  education F4.0
  age F2.0.
MISSING VALUES A1 TO age (9999).
CACHE.
EXECUTE.
DATASET NAME DataSet1 WINDOW=FRONT.

DATASET ACTIVATE DataSet1.

* reverse code first two items

COMPUTE E1.rev = abs(E1 - 7).
COMPUTE E2.rev = abs(E2 - 7).
EXECUTE.

* cronbachs alpha with ICC for 95% confidence intervals

RELIABILITY
  /VARIABLES=E1.rev E2.rev E3 E4 E5
  /SCALE("Cronbach's alpha for 5 extraversion items") ALL
  /MODEL=ALPHA
  /STATISTICS=DESCRIPTIVE SCALE CORR
  /SUMMARY=TOTAL MEANS VARIANCE CORR
  /ICC=MODEL(RANDOM) TYPE(CONSISTENCY) CIN=95 TESTVAL=0.

* factor analysis for informally testing unidimensionality

FACTOR
  /VARIABLES E1.rev E2.rev E3 E4 E5
  /FORMAT SORT.

* load Shrout and Fleiss data

GET DATA  /TYPE=TXT
  /FILE="/Users/nicholasmmichalak/Desktop/analysis-examples/Cronbach's alpha and ICC in R and SPSS/sf.csv"
  /ENCODING='Locale'
  /DELCASE=LINE
  /DELIMITERS=","
  /QUALIFIER='"'
  /ARRANGEMENT=DELIMITED
  /FIRSTCASE=2
  /IMPORTCASE=ALL
  /VARIABLES=
subj F2.0
  J1 F2.0
  J2 F1.0
  J3 F1.0
  J4 F1.0.
CACHE.
EXECUTE.
DATASET NAME DataSet2 WINDOW=FRONT.

DATASET ACTIVATE DataSet2.

* ICC 2-way random effects and averages

RELIABILITY
  /VARIABLES=J1 J2 J3 J4
  /SCALE('ALL VARIABLES') ALL
  /MODEL=ALPHA
  /STATISTICS=DESCRIPTIVE SCALE CORR
  /SUMMARY=TOTAL MEANS VARIANCE CORR
  /ICC=MODEL(RANDOM) TYPE(CONSISTENCY) CIN=95 TESTVAL=0.

* ICC 2-way random effects and absolute agreement

RELIABILITY
  /VARIABLES=J1 J2 J3 J4
  /SCALE('ALL VARIABLES') ALL
  /ICC=MODEL(RANDOM) TYPE(ABSOLUTE) CIN=95 TESTVAL=0.

* ICC 1-way random effects and averages

RELIABILITY
  /VARIABLES=J1 J2 J3 J4
  /SCALE('ALL VARIABLES') ALL
  /ICC=MODEL(ONEWAY) TYPE(CONSISTENCY) CIN=95 TESTVAL=0.

* ICC 1-way random effects and absolute agreement

RELIABILITY
  /VARIABLES=J1 J2 J3 J4
  /SCALE('ALL VARIABLES') ALL
  /ICC=MODEL(ONEWAY) TYPE(ABSOLUTE) CIN=95 TESTVAL=0.

* ICC mixed effects and averages

RELIABILITY
  /VARIABLES=J1 J2 J3 J4
  /SCALE('ALL VARIABLES') ALL
  /ICC=MODEL(MIXED) TYPE(CONSISTENCY) CIN=95 TESTVAL=0.

* ICC mixed effects and absolute agreement

RELIABILITY
  /VARIABLES=J1 J2 J3 J4
  /SCALE('ALL VARIABLES') ALL
  /ICC=MODEL(MIXED) TYPE(ABSOLUTE) CIN=95 TESTVAL=0.
