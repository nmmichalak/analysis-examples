﻿* Encoding: UTF-8.

GET
  FILE='C:\Users\nickmm\Desktop\nmmichalak-analysis-examples-dfd10b4aaf3b\Kleiman et al. (Study '+
    '3).sav'.
DATASET NAME DataSet2 WINDOW=FRONT.

DATASET ACTIVATE DataSet1.
USE ALL.
FILTER BY filter_$.
EXECUTE.

EXAMINE VARIABLES=Bush_Clinton_difference BY Condition
  /PLOT HISTOGRAM NPPLOT BOXPLOT  SPREADLEVEL
  /STATISTICS NONE.

COMPUTE
Bush_Clinton_differenceT = abs(Bush_Clinton_difference)**1.026.
if (Bush_Clinton_difference >= 0)  Bush_Clinton_differenceT =  Bush_Clinton_difference**1.026.

GRAPH
/HISTOGRAM = Bush_Clinton_differenceT.

GLM Bush_Clinton_difference BY Condition
  /METHOD=SSTYPE(1).

GLM Bush_Clinton_difference BY Condition
  /METHOD=SSTYPE(2).

GLM Bush_Clinton_difference BY Condition
  /METHOD=SSTYPE(3).

GLM Bush_Clinton_difference BY Condition
  /METHOD=SSTYPE(3)
  /CONTRAST(Condition) = SPECIAL(-1 1 0
                                          0 1 -1
                                          -1 0 1).

GLM Bush_Clinton_difference BY Condition
  /METHOD=SSTYPE(3)
  /CONTRAST(Condition) = SPECIAL(-1 1 0
                                          0 1 -1
                                          -1 0 1).

OUTPUT EXPORT NAME=Document1
  /CONTENTS  EXPORT=ALL LAYERS=ALL MODELVIEWS=ALL
  /REPORT  DOCUMENTFILE='C:\Users\nickmm\Desktop\nmmichalak-analysis-examples-dfd10b4aaf3b\ANOVA '+
    'in SPSS.htm'
     TITLE=FILENAME FORMAT=HTML RESTYLE=YES.