---
title: "ANOVA in R and SPSS"
author: "Nick Michalak"
date: "June 2, 2016"
output: 
  html_document: 
    code_folding: show
    fig_height: 7.5
    fig_width: 10.5
    keep_md: yes
    theme: readable
    toc: yes
    toc_float:
      collapsed: yes
      smooth_scroll: yes
---

> ### For my first couple posts I'm trying to come up with procedures that are essentially rituals in Psychology. By ritual I mean psychologists do them all the time without really thinking about them and they're the most common Google searches that end with, "...in R?" (e.g., How do I run ANOVAs in R?). Between-subjects ANOVA seems like a good choice. Warning: This post is long because it turns out even basic ANOVA is complicated.

> ### I'll use data from [Klenman, Stern, and Trope (2016)](http://pss.sagepub.com/content/27/3/375) who posted their data on the Open Science Framework [here](https://osf.io/vq4ak/). Specifically, I'll use data from Study 3, which you can download [here](https://osf.io/hsjcy/?action=download&version=1).

### Relevant information about the data (from Klenman, Stern, and Trope, 2016):
> _Metaphorical-conflict manipulation_: "...participants assigned to the compatible condition (n = 136) used their left hand (“Q” key) to categorize a photograph as Clinton and their right hand (“P” key) to categorize a photograph as Bush. Participants assigned to the incompatible condition (n = 136) had the opposite assignment of candidate to response hand. Participants assigned to the control condition (n = 138) used the “T” key to categorize a photograph as Clinton and the “V” key to categorize a photograph as Bush. We chose these two keys because they are vertically aligned, and so using them to categorize liberal and conservative politicians would not prime any metaphorical association between spatial location and political beliefs."

> _Perceptions of candidates’ beliefs_: "To measure perceptions of the former presidents’ ideologies, we had participants separately indicate the social, economic, and general ideologies of Clinton and Bush using scales ranging from 1 to 9 (1 = extremely liberal, 5 = moderate, 9 = extremely conservative). We created a single ideology score for each former president by combining the three ratings (α = .88 and α = .89 for Clinton and Bush, respectively). We then calculated our dependent variable of the perceived difference between the former presidents’ beliefs by subtracting Clinton’s score from Bush’s score. Higher numbers indicate that Clinton’s and Bush’s attitudes were perceived as more different."

### Here's how you do it in SPSS

### Commands used:


### Here's how you do it in R

### Load the data^1^

```{r}

study3_filepath <- "~/Desktop/analysis-examples/Kleiman et al. (Study 3).csv"

study3 <- read.csv(file = study3_filepath, header = TRUE)

study3_subset <- study3[study3$filter_. == "Selected", c("Pnum","Condition","filter_.","Bush_Clinton_difference")]

study3_subset[sample(x = 1:nrow(study3_subset),
                     size = 10,
                     replace = FALSE), ]

```

### Functions used:
* [`read.csv()`](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/read.table.html)
* [`sample()`](http://www.inside-r.org/r-doc/base/sample)
* [`nrow()`](http://www.inside-r.org/r-doc/base/nrow)
* I also use basic subsetting code. You can find an easy introductory tutorial for subsetting at [Quick-R](http://www.statmethods.net/management/subset.html).

### Here's how you do it in SPSS

### Commands used:


### Here's how you do it in R

### First, check the normality assumption of Bush.Clinton.difference.

```{r}
if(!("ggplot2" %in% rownames(installed.packages())))
install.packages("ggplot2")
library(ggplot2)

ggplot(data = study3_subset, aes(x = Bush_Clinton_difference)) + geom_histogram()

```

> ### The difference between average perceptions of Bush's and Clinton's ideologies is, obviously, a difference score: t-tests (thus ANOVAs) assume difference scores are normally distributed. Technically, this variable violates the normality assumption: you can see that in the left-skew of the histogram and when you test this assumption with the Shapiro-Wilk Normality Test. Below I run that test, but note that a test like this has its own asssumptions ripe for violating. In addition, because power to detect an effect increases with sample size, the test will almost certainly produce a significant p-value for large N studies. Don't do this test. It's dumb.

```{r}
with(study3_subset,
  shapiro.test(Bush_Clinton_difference)
)
```

> ### What you can is run what is called a Spread-Level Plot, which, for, "linear models, plots log(abs(studentized residuals) vs. log(fitted values); fits a line to the plot; and calculates a spread-stabilizing transformation from the slope of the line."

> ### That's a lot. But it's prety cool because it turns out that the slope of this model is related to the power (i.e., to the power of...) that will make your data normal: power = 1 - slope.

```{r}

if(!("car" %in% rownames(installed.packages())))
install.packages("car")
library(car)

trnsfrm <- with(study3_subset,
     spreadLevelPlot(Bush_Clinton_difference ~ Condition)
)

trnsfrm

study3_subset$Bush_Clinton_differenceT <- with(study3_subset,
                                               ifelse(Bush_Clinton_difference < 0, abs(Bush_Clinton_difference)^trnsfrm$PowerTransformation,
                                                      Bush_Clinton_difference^trnsfrm$PowerTransformation))

ggplot(data = study3_subset, aes(x = Bush_Clinton_differenceT)) + geom_histogram()
```

> ### Compare this histogram to the untransformed histogram. Pretty friggn' neat. Also, notice I made a new column in the dataset that has this transformed variable, Bush.Clinton.differenceT (T = transformed).

### Functions used
* [`install.packages()`](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/install.packages.html) downloads the name of the package you give it (R has so many packages and people develop more every day) and puts it in your library
* [`rownames()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/row.names.html) outputs the row names of the data frame you give it
* [`%in%`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/match.html) matches the names of elements in one vector with the names of elements in another
* [`installed.packages()`](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/installed.packages.html) gives you a list of all installed packages
* [`library()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/library.html) activates a package in your library. Think of it as taking a package of the shelf.
* [`ggplot()`](https://www.rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf) is the first element of a ggplot2 plot. If you think of ggplot2 plots as sentences with a grammatical structure, then `ggplot()` is the subject...kinda.
* [`geom_histogram()`](http://docs.ggplot2.org/current/geom_histogram.html) adds a histogram to your ggplot
* [`with()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/with.html) lets you call variable names from a dataset without having to include [name of dataframe]$ every time you want to call a variable.
* [`shapiro.test()`](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/shapiro.test.html) conducts a Shapiro-Wilk Normality Test on data you give it.
* [`spreadLevelPlot()`](http://www.inside-r.org/packages/cran/car/docs/spreadLevelPlot) produces a spread-level plot of the data you give it
* [!](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Syntax.html) means "not equal to" in R
* [^](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Syntax.html) is the power operater in R (i.e., "to the power of...")
* [$](https://stat.ethz.ch/R-manual/R-devel/library/base/html/Extract.html) is the extract function in R. It lets you look inside R objects. If an R list "mylist" has two objects in it named "x" and "y", then to access object "y", you'd run mylist$y

### Packages used:
* [_car_](https://cran.r-project.org/web/packages/car/car.pdf) John Fox and colleagues wrote this package of functions to accompany their book Fox, J., & Weisberg, S. (2010). An R companion to applied regression. Thousand Oaks, CA: Sage. This package is a go to for so much that is regression.
* [_ggplot2_](http://ggplot2.org/) is how you plot, and I don't mean just in R. It's how you plot. Period.

### Next, check the homogeneity of variance assumption.

```{r}
ggplot(data = study3_subset, aes(x = Condition, y = Bush_Clinton_difference, color = Condition)) + geom_boxplot() + scale_color_brewer(palette = "Set1")

```

> ### There are a couple points that fall outside the "whiskers" of the boxplots, but there seem to be about the same number of these for each condition. It's fine. Also, don't run the Levene's Test for the same reason I suggested you don't run Shapiro-Wilk: you'd be testing assumptions with tests that have their own assumptions, and big sample sizes can basically guarantee signifigance.

### Between-subjects ANOVA using Type I Sum of Squares

```{r}

# Type I sum of squares
summary(
  aov(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset,
      contrasts = list(Condition = contr.sum(3)))
)
```

> ### Below I introuduce the functions for running models with different types of sums of squares procedures. These models don't have factorial designs, so you don't have to worry about which type to use. I'm merely presenting these because 1) the car package is baller and 2) you should see that the results are the same across types of sums of squares.
> ### For more information regarding types of sums of squares, see Chapter 7 of Maxwell & Delaney (2004)^2^. Or Google it.

### Between-subjects ANOVA using Type II Sum of Squares^3^

```{r}

# Type II sum of squares
Anova(mod = lm(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset,
      contrasts = list(Condition = contr.sum(3))),
  type = "II"
)
```

### Between-subjects ANOVA using Type III Sum of Squares

```{r}
# Type III sum of squares
Anova(mod = lm(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset,
      contrasts = list(Condition = contr.sum(3))),
  type = "III" 
)
```

### Functions used:
* [`Anova()`](http://www.inside-r.org/packages/cran/car/docs/Anova) has more features than the base R statistics function `aov()` (e.g., Type II SS)
* [`aov()`](http://www.inside-r.org/r-doc/stats/aov) is nice for quick and dirty ANOVAs. It comes with R and works well with base R functions like TukeyHSD.
* [`contrasts()`](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/contrasts.html) lets you get contrasts from factors or apply contrasts to factors. You can give a factor contrasts independent of a model or you can set contrasts as an argument in a model function like `lm()`.
* [`list()`](http://www.inside-r.org/r-doc/base/list) makes a list for you. Lists are nice because anything and everything can go in them (e.g., dataframes, other lists, character strings, etc.).
* [`contr.sum()`](http://www.inside-r.org/r-doc/stats/contrast) is a coding system that compares the mean of the dependent variable for a given level to the overall mean of the dependent variable. Give it the numer of levels in your factor.
* [`geom_boxplot()`](http://docs.ggplot2.org/current/geom_boxplot.html) adds a boxplot to your ggplot.
* [`scale_color_brewer()`](http://docs.ggplot2.org/current/scale_brewer.html) allows you to set pre-made or custom color palletes for your ggplot.

### Custom contrasts and 95% confidence intervals

> ### I wanted to test a linear contrast which isn't in the paper, so below I start off by rearranging the order of the factor levels so that the control condition is in the middle and the conflict and congruent conditions are on the ends (i.e., -1, 0, 1 makes sense when the control condition gets the 0 weight). When I test contrasts, R applies weights in the order they're punched in the dataset. 

```{r}
if(!("multcomp" %in% rownames(installed.packages())))
install.packages("multcomp")
library(multcomp)

study3_subset$Condition <- factor(x = study3_subset$Condition, levels = levels(study3_subset$Condition)[c(1, 3, 2)])

levels(study3_subset$Condition)

study3_multcomp <- rbind("CntrlVCnflct" = c(-1, 1, 0),
                          "CntrlVCngrnt" = c(0, 1, -1),
                          "Linear" = c(-1, 0, 1))
  
summary(
    glht(model = lm(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset),
      linfct = mcp(Condition = study3_multcomp),
      alternative = "two.sided"),
    test = adjusted("none"))

confint(glht(model = lm(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset),
      linfct = mcp(Condition = study3_multcomp),
      alternative = "two.sided"),
    test = adjusted("none")
    )

```

> ### Above I tested contrasts in perhaps a more intuitive way: the null hypothesis is that the means of each condition are equal. You might also define the null hypothesis such that all the differences from the grand mean are equal to zero. Below I use the `lm()` function to demonstrate contrasts as deviations from the grand mean. In the beginning I print the contrast weights using `contr.sum()`.

```{r}

contr.sum(3)

summary(
  lm(formula = Bush_Clinton_difference ~ Condition,
     data = study3_subset,
     contrasts = list(Condition = contr.sum(3)))
)

confint(
  lm(formula = Bush_Clinton_difference ~ Condition,
     data = study3_subset,
     contrasts = list(Condition = contr.sum(3)))
)
```


### Functions used:
* [`factor()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/factor.html) turns data you give it into a factor (R's version of SPSS's nominal variable type).
* [`c()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/c.html) combines objects you give it, a lot like Excel's CONCATENATE function but with more applications.
* [`levels()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/levels.html) can be used to set or print the levels of a factor object in R.
* [`rbind()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/cbind.html) stands for row bind: it binds vectors together by rows.
* [`glht()`](http://www.inside-r.org/packages/cran/multcomp/docs/glht) stands for general linear hypothesis. You can use this function for contrasts in model objects (e.g., `lm()`, `aov()`, etc).
* [`confint()`](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/confint.html) gives you confidence intervals for model paramaters. You can use this for CIs for, say, a regression model.

### Packages used:
* [_multcomp_](https://cran.r-project.org/web/packages/multcomp/multcomp.pdf) is a handy package for conducting contrasts.

### Effect size: Eta-squared

> ### I think many psychologists believe that measures of effect size are married to certain tests. For example, you might hear that you use Cohen's d for t-tests, eta-squared or partial eta-squared for F tests and odds ratios for chi-squared tests. In reality, there exist formulas to convert them all into whicever measure you want. I don't think it matters that much which you use, but because I'm demonstrating ANOVA, below I compute eta-squared.

```{r}

if(!("DescTools" %in% rownames(installed.packages())))
install.packages("DescTools")
library(DescTools)

EtaSq(x = aov(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset,
      contrasts = list(Condition = contr.sum(3))),
      type = 2)

```

### Functions used:
* [`EtaSq()`](http://finzi.psych.upenn.edu/library/DescTools/html/EtaSq.html) Calculates eta-squared, partial eta-squared and generalized eta-squared for ANOVAs

### Packages used:
* [_DescTools_](https://cran.r-project.org/web/packages/DescTools/DescTools.pdf) stands for descriptive tools because it's full of functions for descriptive and exploratory procedures

### Effect size: Cohen's d

> ### Because, "Cohen's d is for t-tests."

```{r}

if(!("compute.es" %in% rownames(installed.packages())))
install.packages("compute.es")
library(compute.es)

table(study3_subset$Condition)
levels(study3_subset$Condition)
study3_multcomp

tes(t = as.vector(
  summary(
    glht(model = lm(formula = Bush_Clinton_difference ~ Condition,
      data = study3_subset),
      linfct = mcp(Condition = study3_multcomp),
      alternative = "two.sided"),
    test = adjusted("none"))$test$tstat),
  n.1 = as.vector(
    table(study3_subset$Condition))[c(2, 2, 1)],
  n.2 = as.vector(
    table(study3_subset$Condition))[c(1, 3, 3)],
  verbose = TRUE)
```

### Functions used:
* [`table()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/table.html) is a handy function for creating contingency tables.
* [`tes()`](http://search.r-project.org/library/compute.es/html/tt_to_es.html) takes t-scores and condition sample sizes and gives you all of the effect sizes you'd ever want, with 95% CIs
* [`as.vector()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/vector.html) forces something to be a vector, which in R is basically a line of things. I use it a lot to turn labeled numbers into just the numbers.

### Packages used:
* [_compute.es_](https://cran.r-project.org/web/packages/compute.es/compute.es.pdf) is one of the best packages for computing effect sizes I've ever found, in R and on the web. For example, you can give it p-values and sample sizes and it'll give you all the effect sizes with 95% CIs.

> ### Entire courses are taught on ANOVA, so the best I thought I could do with this post is demonstrate all the procedures you'd need to run a simple one between-subjects factor ANOVA in R and get all the numbers you'd need for a publication. While I was writing this, I realized how many functions I used to get to that point. It might seem like a lot, but remembering all these functions isn't so different from learning the buttons SPSS. I promise you, the coding will pay off when you need to run more complex procedures or when you want to run some creative analysis or customize a plot. Notice, by the way, a small but obvious limitation we've found in SPSS already: computing effect sizes. I noticed less obvious limitations while I was searching the web for SPSS syntax error messages. The R community clearly dwarfs the SPSS community.

> ### But that's it for now. I hope to cover more complex ANOVA designs in future posts. Thanks for reading.

> ### Happy R,

> ### Nick

### Footnotes
1. For the life of me, I could not get the [_foreign_](https://cran.r-project.org/web/packages/foreign/foreign.pdf) package's [`read.spss()`](https://stat.ethz.ch/R-manual/R-devel/library/foreign/html/read.spss.html) or the [_Hmisc_](https://cran.r-project.org/web/packages/Hmisc/Hmisc.pdf) package's [`get.spss()`](http://www.inside-r.org/packages/cran/hmisc/docs/spss.get) to knit in R Markdown without an error even though I was able to run it on the R console. After about 30 minutes, I just wrote the .sav file into a .csv I that's the object I use throughout the post.
2. Maxwell, S. E., & Delaney, H. D. (2004). Designing experiments and analyzing data: A model comparison perspective. New York, NY: Psychology Press
3. This SS procedure is equivalent to SPSS's Type III SS. Yes, Type II is Type II. Up is down and black is white. There is no obvious consistency for naming SS types so I suggest learning how the methods differ rather than how they're named.