library(MASS)

# set randomizer for reproducing same results later
set.seed(1234)

# simulate data: 10 items, 3 targets = 30 columns + id column
dat <- mvrnorm(n = 100,
               mu = rep(c(3.5,4.0,4.5),
                        times = 9),
               Sigma = diag(x = .5, nrow = 27, ncol = 27) + .5)

# add id column and convert to dataframe
dat.wide <- data.frame(cbind(1:nrow(dat),dat))

# name columns
names(dat.wide) <- c("id", paste0(c("bel.", "loy.", "thrd."), rep(1:9, each = 3)))

# long format conversion
library(reshape2)
dat.long <- melt(data = dat.wide,
                 id.vars = "id",
                 measure.vars = names(dat.wide[,-1]),
                 variable.name = "column",
                 value.name = "judgment")

# identify response items
dat.long$item <- with(data = dat.long,
ifelse(grepl(pattern = "bel.1", x = column, fixed = TRUE) == TRUE | grepl(pattern = "loy.1", x = column, fixed = TRUE) == TRUE | grepl(pattern = "thrd.1", x = column, fixed = TRUE) == TRUE, "item 1",
ifelse(grepl(pattern = ".2", x = column, fixed = TRUE) == TRUE, "item 2",
ifelse(grepl(pattern = ".3", x = column, fixed = TRUE) == TRUE, "item 3",
ifelse(grepl(pattern = ".4", x = column, fixed = TRUE) == TRUE, "item 4",
ifelse(grepl(pattern = ".5", x = column, fixed = TRUE) == TRUE, "item 5",
ifelse(grepl(pattern = ".6", x = column, fixed = TRUE) == TRUE, "item 6",
ifelse(grepl(pattern = ".7", x = column, fixed = TRUE) == TRUE, "item 7",
ifelse(grepl(pattern = ".8", x = column, fixed = TRUE) == TRUE, "item 8",
ifelse(grepl(pattern = ".9", x = column, fixed = TRUE) == TRUE, "item 9", NA))))))))))

# identify voter targets with new column
dat.long$voter <- with(data = dat.long, ifelse(grepl(pattern = "bel", x = column, fixed = TRUE) == TRUE, "believer",
                                       ifelse(grepl(pattern = "loy", x = column, fixed = TRUE) == TRUE, "loyalist",
                                       ifelse(grepl(pattern = "thrd", x = column, fixed = TRUE) == TRUE, "third party",NA))))

# create function for adjusted values
adjust.values <- function(long.data, id.col, dv.col, resp.col){
  
  # call the data 'x' (easier to type)
  x = data.frame(id.col = long.data[,id.col],
                 item.col = long.data[,dv.col],
                 value.col = long.data[,resp.col])
  
  # names of unique items
  items = unique(x[,"item.col"])
  
  # create empty list to store subsetted dataframe with adjusted column
  adjust.cols = list()
  for(i in 1:length(items)){
    
  # subset
    sub =  x[x$item.col == items[i],]
    
  # id mean
    sub$id.mean = with(data = sub, by(data = value.col,
                                        INDICES = id.col,
                                        FUN = mean))
    
  # grand mean
    grand.mean = with(data = sub, mean(value.col,
                                       na.rm = TRUE))
    
  # adjusted values
    sub$value.col.adj = with(data = sub,
                             value.col - id.mean + grand.mean)
    
  # store in list and repeat
    adjust.cols[[i]] = sub
    
  }
  
  # new dataframe with the adjusted column of values
  long.data.adj = do.call(what = rbind, args = adjust.cols)
  
  names(long.data.adj) = c(id.col, dv.col, resp.col, 'id.mean', paste0(resp.col,".adj"))
  
  # output
    return(long.data.adj)
}

# try it out
# function takes 4 arguments
# data.long = your data
# id.col = 'name of your id column
# dv.col = 'name of your column of dv names'
# resp.col = 'name of your column of responses to those dvs'
# outputs data frame with new column of adjusted values
dat.long.adj <- adjust.values(long.data = dat.long,
                              id.col = 'id',
                              dv.col = 'item',
                              resp.col = 'judgment')

# add the column you need to your old dataset
dat.long$judgment.adj <- dat.long.adj$judgment.adj

library(ggplot2)

# with the adjusted values
ggplot(data = dat.long, aes(x = item, y = judgment.adj, fill = voter)) +
  stat_summary(fun.data = 'mean_cl_normal', geom = "errorbar", width = 0.1) +
  stat_summary(fun.data = 'mean_cl_normal', geom = "point", color = 'black', shape = 21, size = 3.5) +
  scale_y_continuous(breaks = seq(-1,9,1), limits = c(-1,9), labels = c('not at all', 0, 1, 2, 3, 4, 5, 6, 7, 8, 'very much')) +
  coord_cartesian(ylim = c(-1,9)) +
  coord_flip() +
  labs(x = 'dv item', y = 'judgment (adjusted)', fill = 'voter type') +
  theme_minimal(base_size = 18) +
  scale_fill_brewer(palette = 'Greys') +
  theme(axis.title.y = element_text(colour = 'black'),
        axis.title.x = element_text(colour = 'black'),
        axis.text.x = element_text(colour = 'black'),
        axis.text.y = element_text(colour = 'black'),
        legend.text = element_text(colour = 'black'),
        legend.position = 'top')

# without the adjusted values
ggplot(data = dat.long, aes(x = item, y = judgment, fill = voter)) +
  stat_summary(fun.data = 'mean_cl_normal', geom = "errorbar", width = 0.1) +
  stat_summary(fun.data = 'mean_cl_normal', geom = "point", color = 'black', shape = 21, size = 3.5) +
  scale_y_continuous(breaks = seq(-1,9,1), limits = c(-1,9), labels = c('not at all', 0, 1, 2, 3, 4, 5, 6, 7, 8, 'very much')) +
  coord_cartesian(ylim = c(-1,9)) +
  coord_flip() +
  labs(x = 'dv item', y = 'judgment (not adjusted)', fill = 'voter type') +
  theme_minimal(base_size = 18) +
  scale_fill_brewer(palette = 'Greys') +
  theme(axis.title.y = element_text(colour = 'black'),
        axis.title.x = element_text(colour = 'black'),
        axis.text.x = element_text(colour = 'black'),
        axis.text.y = element_text(colour = 'black'),
        legend.text = element_text(colour = 'black'),
        legend.position = 'top')

# check descriptives
library(psych)
describeBy(x = dat.long[,c('judgment','judgment.adj')],
           group = dat.long[,c('voter','item')],
           mat = TRUE)