# load required packages

# packages I want
want_packages <- c("lavaan", "dplyr")

# packages that are already downloaded in my library
have_packages   <- want_packages %in% rownames(installed.packages())

# if I don't have these packages, download the ones I don't have among the ones I want
if(any(!have_packages)) install.packages(want_packages[!have_packages])

# load psych package from library
library(lavaan)
library(dplyr)


# save covariance matrix from Wheaton et al. (1977)


# The classic Wheaton et. al. (1977) model
# panel data on he stability of alienation
lower <- '
11.834,
6.947, 9.364,
6.819, 5.091, 12.532,
4.783, 5.028, 7.495, 9.986,
-3.839, -3.889, -3.841, -3.625, 9.610,
-21.899, -18.831, -21.748, -18.775, 35.522, 450.288 '

# convert to a full symmetric covariance matrix with names
wheaton.cov <- getCov(lower, names = paste0("V",1:6))


# Figure 1 model
# V1 and V3 estimates should be 1
# V2 and V4 estimates should be free
# cov for V1 and V3 should be equal
# cov for V2 and V4 should be equal

figure1 <- '
# latent variables
L1 =~ V1 + I1*V2
L2 =~ V3 + I1*V4
# covariances
V1 ~~ a*V1
V2 ~~ b*V2
V3 ~~ a*V3
V4 ~~ b*V4
V3 ~~ co*V1
V4 ~~ co*V2
L1 ~~ lat.var*L1
L2 ~~ lat.var*L2
'

# fit
fit1 <- cfa(figure1, 
            sample.cov = wheaton.cov, 
            sample.nobs = 932)

# summary
summary(fit1, standardized = TRUE,
        fit.measures = TRUE)

fitMeasures(fit1)

# Figure 2 model
# free estimate all exogenous
# latent 1 and laten 2 should be 1
# exogenous var for V1 and V3 should be equal
# exogenous var for V2 and V4 should be equal

figure2 <- '
# latent variables
L1 =~ NA*V1 + V2
L2 =~ NA*V3 + V4
# covariances
V1 ~~ a*V1
V2 ~~ b*V2
V3 ~~ a*V3
V4 ~~ b*V4
L1 ~~ 1*L1
L2 ~~ 1*L2
'

# fit
fit2 <- cfa(figure2, 
            sample.cov = wheaton.cov, 
            sample.nobs = 932)

# summary
summary(fit2, standardized = TRUE,
        fit.measures = TRUE)

fitMeasures(fit2)
