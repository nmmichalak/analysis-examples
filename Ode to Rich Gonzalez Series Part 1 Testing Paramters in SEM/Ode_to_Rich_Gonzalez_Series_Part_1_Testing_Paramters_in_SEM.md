# Ode to Rich Gonzalez Series Part I: Testing Parameters in SEM
Nick Michalak  
August 30, 2016  

# load required packages

```r
# packages I want
want_packages <- c("lavaan", "dplyr")

# packages that are already downloaded in my library
have_packages   <- want_packages %in% rownames(installed.packages())

# if I don't have these packages, download the ones I don't have among the ones I want
if(any(!have_packages)) install.packages(want_packages[!have_packages])

# load psych package from library
library(lavaan)
library(dplyr)
```

# save covariance matrix from Wheaton et al. (1977)


```r
# The classic Wheaton et. al. (1977) model
# panel data on he stability of alienation
lower <- '
11.834,
6.947, 9.364,
6.819, 5.091, 12.532,
4.783, 5.028, 7.495, 9.986,
-3.839, -3.889, -3.841, -3.625, 9.610,
-21.899, -18.831, -21.748, -18.775, 35.522, 450.288 '

# convert to a full symmetric covariance matrix with names
wheaton.cov <- getCov(lower, names = paste0("V",1:6))
```

# Figure 1 model


```r
figure1 <- '
  # latent variables
    L1 =~ V1 + V2
    L2 =~ V3 + equal("L1 =~ V2")*V4

  # covariances
    V1 ~~ a*V1
    V2 ~~ b*V2
    V3 ~~ a*V3
    V4 ~~ b*V4
    L1 ~~ c*L1
    L2 ~~ c*L2'

# fit
fit1 <- cfa(figure1, 
           sample.cov = wheaton.cov, 
           sample.nobs = 932)

# summary
summary(fit1, standardized = TRUE,
        fit.measures = TRUE)
```

```
## lavaan (0.5-20) converged normally after  36 iterations
## 
##   Number of observations                           932
## 
##   Estimator                                         ML
##   Minimum Function Test Statistic               63.937
##   Degrees of freedom                                 5
##   P-value (Chi-square)                           0.000
## 
## Model test baseline model:
## 
##   Minimum Function Test Statistic             1565.624
##   Degrees of freedom                                 6
##   P-value                                        0.000
## 
## User model versus baseline model:
## 
##   Comparative Fit Index (CFI)                    0.962
##   Tucker-Lewis Index (TLI)                       0.955
## 
## Loglikelihood and Information Criteria:
## 
##   Loglikelihood user model (H0)              -8981.349
##   Loglikelihood unrestricted model (H1)      -8949.380
## 
##   Number of free parameters                          5
##   Akaike (AIC)                               17972.697
##   Bayesian (BIC)                             17996.884
##   Sample-size adjusted Bayesian (BIC)        17981.004
## 
## Root Mean Square Error of Approximation:
## 
##   RMSEA                                          0.112
##   90 Percent Confidence Interval          0.089  0.138
##   P-value RMSEA <= 0.05                          0.000
## 
## Standardized Root Mean Square Residual:
## 
##   SRMR                                           0.036
## 
## Parameter Estimates:
## 
##   Information                                 Expected
##   Standard Errors                             Standard
## 
## Latent Variables:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   L1 =~                                                                 
##     V1                1.000                               2.946    0.844
##     V2      (.p2.)    0.831    0.029   28.640    0.000    2.449    0.788
##   L2 =~                                                                 
##     V3                1.000                               2.946    0.844
##     V4      (L1=~)    0.831    0.029   28.640    0.000    2.449    0.788
## 
## Covariances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   L1 ~~                                                                 
##     L2                6.463    0.449   14.392    0.000    0.745    0.745
## 
## Variances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     V1         (a)    3.492    0.272   12.850    0.000    3.492    0.287
##     V2         (b)    3.669    0.208   17.599    0.000    3.669    0.380
##     V3         (a)    3.492    0.272   12.850    0.000    3.492    0.287
##     V4         (b)    3.669    0.208   17.599    0.000    3.669    0.380
##     L1         (c)    8.678    0.501   17.306    0.000    1.000    1.000
##     L2         (c)    8.678    0.501   17.306    0.000    1.000    1.000
```

```r
fitMeasures(fit1)
```

```
##                npar                fmin               chisq 
##               5.000               0.034              63.937 
##                  df              pvalue      baseline.chisq 
##               5.000               0.000            1565.624 
##         baseline.df     baseline.pvalue                 cfi 
##               6.000               0.000               0.962 
##                 tli                nnfi                 rfi 
##               0.955               0.955               0.951 
##                 nfi                pnfi                 ifi 
##               0.959               0.799               0.962 
##                 rni                logl   unrestricted.logl 
##               0.962           -8981.349           -8949.380 
##                 aic                 bic              ntotal 
##           17972.697           17996.884             932.000 
##                bic2               rmsea      rmsea.ci.lower 
##           17981.004               0.112               0.089 
##      rmsea.ci.upper        rmsea.pvalue                 rmr 
##               0.138               0.000               0.381 
##          rmr_nomean                srmr        srmr_bentler 
##               0.381               0.036               0.036 
## srmr_bentler_nomean         srmr_bollen  srmr_bollen_nomean 
##               0.036               0.028               0.028 
##          srmr_mplus   srmr_mplus_nomean               cn_05 
##               0.034               0.034             162.374 
##               cn_01                 gfi                agfi 
##             220.912               0.968               0.935 
##                pgfi                 mfi                ecvi 
##               0.484               0.969               0.079
```

# Figure 2 model


```r
figure2 <- '
  # latent variables
    L1 =~ NA*V1 + V2
    L2 =~ NA*V3 + equal("L1 =~ V2")*V4

  # covariances
    V1 ~~ a*V1
    V2 ~~ b*V2
    V3 ~~ a*V3
    V4 ~~ b*V4
    L1 ~~ 1*L1
    L2 ~~ 1*L2'

# fit
fit2 <- cfa(figure2, 
           sample.cov = wheaton.cov, 
           sample.nobs = 932)

# summary
summary(fit2, standardized = TRUE,
        fit.measures = TRUE)
```

```
## lavaan (0.5-20) converged normally after  22 iterations
## 
##   Number of observations                           932
## 
##   Estimator                                         ML
##   Minimum Function Test Statistic               63.134
##   Degrees of freedom                                 4
##   P-value (Chi-square)                           0.000
## 
## Model test baseline model:
## 
##   Minimum Function Test Statistic             1565.624
##   Degrees of freedom                                 6
##   P-value                                        0.000
## 
## User model versus baseline model:
## 
##   Comparative Fit Index (CFI)                    0.962
##   Tucker-Lewis Index (TLI)                       0.943
## 
## Loglikelihood and Information Criteria:
## 
##   Loglikelihood user model (H0)              -8980.947
##   Loglikelihood unrestricted model (H1)      -8949.380
## 
##   Number of free parameters                          6
##   Akaike (AIC)                               17973.895
##   Bayesian (BIC)                             18002.919
##   Sample-size adjusted Bayesian (BIC)        17983.863
## 
## Root Mean Square Error of Approximation:
## 
##   RMSEA                                          0.126
##   90 Percent Confidence Interval          0.100  0.154
##   P-value RMSEA <= 0.05                          0.000
## 
## Standardized Root Mean Square Residual:
## 
##   SRMR                                           0.032
## 
## Parameter Estimates:
## 
##   Information                                 Expected
##   Standard Errors                             Standard
## 
## Latent Variables:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   L1 =~                                                                 
##     V1                2.897    0.101   28.798    0.000    2.897    0.840
##     V2      (.p2.)    2.448    0.075   32.488    0.000    2.448    0.787
##   L2 =~                                                                 
##     V3                2.994    0.102   29.440    0.000    2.994    0.848
##     V4      (L1=~)    2.448    0.075   32.488    0.000    2.448    0.787
## 
## Covariances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##   L1 ~~                                                                 
##     L2                0.745    0.024   30.522    0.000    0.745    0.745
## 
## Variances:
##                    Estimate  Std.Err  Z-value  P(>|z|)   Std.lv  Std.all
##     V1         (a)    3.489    0.271   12.859    0.000    3.489    0.294
##     V2         (b)    3.672    0.208   17.630    0.000    3.672    0.380
##     V3         (a)    3.489    0.271   12.859    0.000    3.489    0.280
##     V4         (b)    3.672    0.208   17.630    0.000    3.672    0.380
##     L1                1.000                               1.000    1.000
##     L2                1.000                               1.000    1.000
```

```r
fitMeasures(fit2)
```

```
##                npar                fmin               chisq 
##               6.000               0.034              63.134 
##                  df              pvalue      baseline.chisq 
##               4.000               0.000            1565.624 
##         baseline.df     baseline.pvalue                 cfi 
##               6.000               0.000               0.962 
##                 tli                nnfi                 rfi 
##               0.943               0.943               0.940 
##                 nfi                pnfi                 ifi 
##               0.960               0.640               0.962 
##                 rni                logl   unrestricted.logl 
##               0.962           -8980.947           -8949.380 
##                 aic                 bic              ntotal 
##           17973.895           18002.919             932.000 
##                bic2               rmsea      rmsea.ci.lower 
##           17983.863               0.126               0.100 
##      rmsea.ci.upper        rmsea.pvalue                 rmr 
##               0.154               0.000               0.328 
##          rmr_nomean                srmr        srmr_bentler 
##               0.328               0.032               0.032 
## srmr_bentler_nomean         srmr_bollen  srmr_bollen_nomean 
##               0.032               0.028               0.028 
##          srmr_mplus   srmr_mplus_nomean               cn_05 
##               0.031               0.031             141.060 
##               cn_01                 gfi                agfi 
##             196.994               0.968               0.920 
##                pgfi                 mfi                ecvi 
##               0.387               0.969               0.081
```
