# t-tests, for() loops, and p-curves in R (and why small p-values matter)
Nick Michalak  
August 23, 2016  

## simulated results for quiz


```r
# list of p-values for n = 150
ps.150 <- list()

# list of p-values for n = 30
ps.30 <- list()

# list of results for small effect
result.sml <- list()

# list of results for large effect
result.lrg <- list()
for(i in 1:10){
  # delta for n = 150
  d.sml <- power.t.test(n = 150, power = .8)$delta
  
  # delta for n = 30
  d.lrg <- power.t.test(n = 30, power = .8)$delta
  
  # t-test for small effect
  test.sml <- t.test(rnorm(150,0,1), rnorm(150,d.sml,1), var.equal = T)
  
  # t-test for large effect
  test.lrg <- t.test(rnorm(30,0,1), rnorm(30,d.lrg,1), var.equal = T)
  
  # store each p-value in loop
  ps.150[[i]] <- test.sml$p.value
  ps.30[[i]] <- test.lrg$p.value
  
  # create results statements (Study i: t() = , p = )
  result.sml[[i]] <- paste0("Study ", i,": t(",test.sml$parameter,") = ",round(test.sml$statistic,2),", p = ",test.sml$p.value)
  result.lrg[[i]] <- paste0("Study ", i,": t(",test.lrg$parameter,") = ",round(test.lrg$statistic,2),", p = ",test.lrg$p.value)
}
```

## small effect size results

```r
result.sml
```

```
## [[1]]
## [1] "Study 1: t(298) = -0.53, p = 0.598657798550928"
## 
## [[2]]
## [1] "Study 2: t(298) = -2.02, p = 0.0444880307491955"
## 
## [[3]]
## [1] "Study 3: t(298) = -2.62, p = 0.00927502918506702"
## 
## [[4]]
## [1] "Study 4: t(298) = -0.87, p = 0.382702463324688"
## 
## [[5]]
## [1] "Study 5: t(298) = -1.49, p = 0.138043151303553"
## 
## [[6]]
## [1] "Study 6: t(298) = -2.7, p = 0.00735853260487822"
## 
## [[7]]
## [1] "Study 7: t(298) = -5.05, p = 7.74037434680722e-07"
## 
## [[8]]
## [1] "Study 8: t(298) = -2.59, p = 0.0101483172253008"
## 
## [[9]]
## [1] "Study 9: t(298) = -3.68, p = 0.000271631856533488"
## 
## [[10]]
## [1] "Study 10: t(298) = -2.72, p = 0.00682903022629266"
```

## only sig p-values for small effect tested in 10 studies


```r
unlist(ps.150)[unlist(ps.150) < .05]
```

```
## [1] 4.448803e-02 9.275029e-03 7.358533e-03 7.740374e-07 1.014832e-02
## [6] 2.716319e-04 6.829030e-03
```

## histogram for small effect tested in 10 studies


```r
hist.150 <- hist(unlist(ps.150)[unlist(ps.150) < .05], plot = FALSE)
hist.150$counts <- round(hist.150$counts/sum(hist.150$counts),3)
plot(hist.150, freq = TRUE,
     xlab = paste(length(unlist(ps.150)[unlist(ps.150) < .05]), "significant p-values (p < .05)"),
     ylab = "Relative Frequency of p-values",
     labels = TRUE)
```

![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-4-1.png)<!-- -->

## large effect size results


```r
result.lrg
```

```
## [[1]]
## [1] "Study 1: t(58) = -1.95, p = 0.0564663270129574"
## 
## [[2]]
## [1] "Study 2: t(58) = -2.88, p = 0.00553932246400419"
## 
## [[3]]
## [1] "Study 3: t(58) = -3.54, p = 0.000801923131656802"
## 
## [[4]]
## [1] "Study 4: t(58) = -2.38, p = 0.0206402918557408"
## 
## [[5]]
## [1] "Study 5: t(58) = -1.89, p = 0.0641646268570817"
## 
## [[6]]
## [1] "Study 6: t(58) = -2.67, p = 0.00984949672212303"
## 
## [[7]]
## [1] "Study 7: t(58) = -3.03, p = 0.00364303270391892"
## 
## [[8]]
## [1] "Study 8: t(58) = -2.57, p = 0.0127301536969599"
## 
## [[9]]
## [1] "Study 9: t(58) = -3.43, p = 0.00111869926209941"
## 
## [[10]]
## [1] "Study 10: t(58) = -2.1, p = 0.0396834250288522"
```

## only sig p-values for large effect tested in 10 studies


```r
unlist(ps.30)[unlist(ps.30) < .05]
```

```
## [1] 0.0055393225 0.0008019231 0.0206402919 0.0098494967 0.0036430327
## [6] 0.0127301537 0.0011186993 0.0396834250
```

## histogram for large effect tested in 10 studies


```r
hist.30 <- hist(unlist(ps.30)[unlist(ps.30) < .05], plot = FALSE)
hist.30$counts <- round(hist.30$counts/sum(hist.30$counts),3)
plot(hist.30, freq = TRUE,
     xlab = paste(length(unlist(ps.30)[unlist(ps.30) < .05]), "significant p-values (p < .05)"),
     ylab = "Relative Frequency of p-values",
     labels = TRUE)
```

![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-7-1.png)<!-- -->

## basic for() loop


```r
for(i in 1:10){
  print("I <3 statistics")
}
```

```
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
## [1] "I <3 statistics"
```

## slightly more complicated for() loop

```r
msg <- c("I", "<3", "statistics")

# print it
msg
```

```
## [1] "I"          "<3"         "statistics"
```

```r
# how many elements (how long) is msg?
length(msg)
```

```
## [1] 3
```

```r
# print each element using for() loop
for(i in 1:length(msg)){
  print(msg[[i]])
}
```

```
## [1] "I"
## [1] "<3"
## [1] "statistics"
```

## t-test: mean is equal to zero? 


```r
t.test(x = rnorm(n = 100, mean = 0, sd = 1), mu = 0)
```

```
## 
## 	One Sample t-test
## 
## data:  rnorm(n = 100, mean = 0, sd = 1)
## t = 1.0304, df = 99, p-value = 0.3053
## alternative hypothesis: true mean is not equal to 0
## 95 percent confidence interval:
##  -0.1055099  0.3334730
## sample estimates:
## mean of x 
## 0.1139815
```

## t-test: mean is equal to one? 


```r
t.test(x = rnorm(n = 100, mean = 0, sd = 1), mu = 1)
```

```
## 
## 	One Sample t-test
## 
## data:  rnorm(n = 100, mean = 0, sd = 1)
## t = -9.8806, df = 99, p-value < 2.2e-16
## alternative hypothesis: true mean is not equal to 1
## 95 percent confidence interval:
##  -0.1020263  0.2665699
## sample estimates:
##  mean of x 
## 0.08227179
```

## t-test: extract the p-value


```r
t.test(x = rnorm(n = 100, mean = 0, sd = 1), mu = 1)$p.value
```

```
## [1] 1.900054e-16
```

## use the for() loop to demonstrate how often we find the mean is sig. different than zero


```r
# save the results in an empty list called p-values
pvalues <- list()
for(i in 1:10000){
  pvalues[[i]] = t.test(x = rnorm(n = 100, mean = 0, sd = 1), mu = 0)$p.value
}
```

## mean of those p-values < .05
> since the true mean is zero and the alpha level (read: error rate) is 5%, we should expect 5% of the p-values to be less than .05


```r
mean(unlist(pvalues < .05))
```

```
## [1] 0.0486
```

## we can actually test whether this is different than .05


```r
# how many p-values are < .05?
x <- length(unlist(pvalues)[unlist(pvalues) < .05])

# how many p-values?
n <- length(unlist(pvalues))

# proportion test, where x =  successes, n = all outcomes, and p = test value (the probability of success)
prop.test(x = x, n = n, p = .05)
```

```
## 
## 	1-sample proportions test with continuity correction
## 
## data:  x out of n, null probability 0.05
## X-squared = 0.38368, df = 1, p-value = 0.5356
## alternative hypothesis: true p is not equal to 0.05
## 95 percent confidence interval:
##  0.04450814 0.05304264
## sample estimates:
##      p 
## 0.0486
```

## p-curve simulation function


```r
p.curves <- function(sims = 10000, delta = .4, power = .80, n.more = 10, max.hack.n.mult = 2){
  
  # run power analysis to detrmine n for given power and delta
  pwr.analysis = power.t.test(delta = delta,
               power = power,
               sig.level = .05)
  n = pwr.analysis$n

  # empty lists for null and true difference results
null <- list()
real <- list()
for(i in 1:sims){
  # baseline group 1
  group1 = rnorm(n = n, mean = 0, sd = 1)
  
  # add true delta
  group2 = rnorm(n = n, mean = 0 + delta, sd = 1)
  
  # comparison for null effects (same population as group 1)
  group3 = rnorm(n = n, mean = 0, sd = 1)
  
  # t-tests
  null[[i]] = t.test(group1, group3)$p.value
  real[[i]] = t.test(group1, group2)$p.value
}

# histogram of null relationship p-values
null.hist = hist(x = unlist(null)[unlist(null) < .05], plot = FALSE)
null.hist$counts <- round(null.hist$counts/sum(null.hist$counts),3)
plot(null.hist,
     freq = TRUE,
     main = paste(sims, "independent t-tests (n =",round(n),", delta =", 0, ")"),xlab = paste(length(unlist(null)[unlist(null) < .05]), "significant p-values (p < .05)"),
     ylab = "Relative frequency of p-values",
     labels = TRUE)
  
# histogram of true difference delta p-values
real.hist = hist(x = unlist(real)[unlist(real) < .05], plot = FALSE)
real.hist$counts <- round(real.hist$counts/sum(real.hist$counts),3)
plot(real.hist,
     freq = TRUE,
     main = paste(sims, "independent t-tests (n =",round(n),", delta =", delta, ", power =", power, ")"),
     xlab = paste(length(unlist(real)[unlist(real) < .05]), "significant p-values (p < .05)"),
     ylab = "Relative frequency of p-values",
     labels = TRUE)

# empty list for storing p-hacked results
phacked <- list()
for(i in 1:sims){
  
  # baseline group 1
  group1 = rnorm(n = n, mean = 0, sd = 1)
  
  # same population as group 1
  group2 = rnorm(n = n, mean = 0, sd = 1)
  
  # while test results is non-significant and group 1 is smaller than the max group size allowed, append n.more random values from same populations as above
  while((t.test(group1, group2)$p.value >= .05 & length(group1) < n * max.hack.n.mult) == TRUE){
    group1 = append(group1, rnorm(n = n.more, mean = 0, sd = 1))
    group2 = append(group2, rnorm(n = n.more, mean = 0, sd = 1))
  }
  
  # now store t-tests if above is FALSE
  phacked[[i]] = t.test(group1, group2)$p.value
}

# histogram of p-hacked comparisons
phacked.hist = hist(x = unlist(phacked)[unlist(phacked) < .05], plot = FALSE)
phacked.hist$counts = round(phacked.hist$counts/sum(phacked.hist$counts),3)
plot(phacked.hist,
     main = paste(sims, "independent, p-hacked t-tests (n =",round(n),", delta =", 0, ")"),
     xlab = paste(length(unlist(phacked)[unlist(phacked) < .05]), "significant p-values (p < .05)"),
     ylab = "Relative frequency of p-values",
     freq = TRUE,
     labels = TRUE)
  
}
```

## test 33%, 50%, and 80% power


```r
lapply(c(1/3,1/2,4/5), function(x) {
  deltas <- c(.1, .3, .5)
  for(i in 1:length(deltas)){
    p.curves(power = x, delta = deltas[[i]])
  }
}
)
```

![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-1.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-2.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-3.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-4.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-5.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-6.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-7.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-8.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-9.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-10.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-11.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-12.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-13.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-14.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-15.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-16.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-17.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-18.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-19.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-20.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-21.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-22.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-23.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-24.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-25.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-26.png)<!-- -->![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-17-27.png)<!-- -->

```
## [[1]]
## NULL
## 
## [[2]]
## NULL
## 
## [[3]]
## NULL
```

## simulate p-values resulting from differently powered samples


```r
power.sim <- sapply(c(1/3, 1/2, 4/5), function(x){
  delta = .4
  pwr.analysis = power.t.test(delta = delta,
               power = x,
               sig.level = .05)
  n = pwr.analysis$n
  p.values <- list()
  for(i in 1:10000){
    p.values[[i]] <- t.test(rnorm(n = n, mean = 0, sd = 1), rnorm(n = n, mean = 0 + delta, sd = 1))$p.value
  }
  return(p.values)
}, simplify = FALSE, USE.NAMES = TRUE
)
```

## all p-values


```r
if(any((c("ggplot2", "dplyr") %in% row.names(installed.packages()))) == FALSE)
install.packages(c("ggplot2", "dplyr")[c("ggplot2", "dplyr") %in% row.names(installed.packages()) == FALSE])
library(ggplot2)
library(dplyr)

data.frame(power = gl(n = 3, k = 10000, length = 30000, labels = c("33%", "50%", "80%")), p.value = unlist(power.sim)) %>% ggplot(., aes(x = p.value, fill = power)) +
  geom_histogram(bins = 80) +
  scale_fill_brewer(palette = "Reds") +
  labs(x = "p-values", y = "Frequency", title = "10000 simulations (delta = 0.4)")
```

![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-19-1.png)<!-- -->

## only p-values < .05


```r
data.frame(power = gl(n = 3, k = 10000, length = 30000, labels = c("33%", "50%", "80%")), p.value = unlist(power.sim)) %>% filter(p.value < .05) %>% ggplot(., aes(x = p.value, fill = power)) +
  geom_histogram(bins = 80) +
  scale_fill_brewer(palette = "Reds") + labs(x = "p-values < .05", y = "Frequency", title = "10000 simulations (delta = 0.4)")
```

![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-20-1.png)<!-- -->

## simulated results for phack example


```r
phack.ps <- list()
phack.result <- list()
for(i in 1:10){
  sml.d <- power.t.test(n = 100, power = 1/2)$delta
  control = rnorm(100,0,1)
  exp = rnorm(100,0+sml.d,1)
  test = t.test(control, exp, var.equal = T)
  while((test$p.value >= .05 & length(control) < 200) == TRUE){
    control = append(control, rnorm(10,0,1))
    exp = append(exp, rnorm(10,0+sml.d,1))
  }
  phack.ps[[i]] <- test$p.value
  phack.result[[i]] <- paste0("Study ", i,": t(",test$parameter,") = ",round(test$statistic,2),", p = ",test$p.value)
}
```

## small effect size results that were p-hacked


```r
phack.result
```

```
## [[1]]
## [1] "Study 1: t(198) = -1.14, p = 0.256328028820549"
## 
## [[2]]
## [1] "Study 2: t(198) = -3.76, p = 0.000223464219139729"
## 
## [[3]]
## [1] "Study 3: t(198) = -2.72, p = 0.00715484193682965"
## 
## [[4]]
## [1] "Study 4: t(198) = -3.88, p = 0.000142988182061499"
## 
## [[5]]
## [1] "Study 5: t(198) = -4.1, p = 6.05110334564295e-05"
## 
## [[6]]
## [1] "Study 6: t(198) = -1.3, p = 0.195890658116213"
## 
## [[7]]
## [1] "Study 7: t(198) = -1.2, p = 0.232931856538157"
## 
## [[8]]
## [1] "Study 8: t(198) = -2.57, p = 0.0109026030641378"
## 
## [[9]]
## [1] "Study 9: t(198) = -0.61, p = 0.543965422874521"
## 
## [[10]]
## [1] "Study 10: t(198) = -0.98, p = 0.327795801799593"
```

## only sig p-values for small effect tested in 10 studies


```r
unlist(phack.ps)[unlist(phack.ps) < .05]
```

```
## [1] 2.234642e-04 7.154842e-03 1.429882e-04 6.051103e-05 1.090260e-02
```

## histogram for small effect tested in 10 studies


```r
phack.hist = hist(unlist(phack.ps)[unlist(phack.ps) < .05], plot = FALSE)
phack.hist$counts = round(phack.hist$counts/sum(phack.hist$counts),3)
plot(phack.hist,
     freq = TRUE,
     xlab = paste(length(unlist(phack.ps)[unlist(phack.ps) < .05]), "significant p-values (p < .05)"),
     ylab = "Relative Frequency of p-values",
     labels = TRUE)
```

![](t-tests,_for___loops,_and_p-curves__and_why_small_p-values_matter__files/figure-html/unnamed-chunk-24-1.png)<!-- -->
