# Repeated measures contrasts in R and SPSS
Nick Michalak  
June 1st, 2016  

> ### This is my first post so I'm going to demonstrate something simple: testing the effects of one within-subjects factor. My goal in this post and others is to provide simple step-by-step procedures for conducting analyses in R that you would have otherwise conducted in SPSS.

### Here's where to find the data^1^: [UCLA SPSS Repeated Measures tutorial](http://www.ats.ucla.edu/stat/spss/seminars/Repeated_Measures/)

> ### "The data consists of people who were randomly assigned to two different diets: low-fat and not low-fat and three different types of exercise: at rest, walking leisurely and running. Their pulse rate was measured at three different time points during their assigned exercise: at 1 minute, 15 minutes and 30 minutes." I'll use the other factors in this data in future posts demonstrating more complex concepts and procedures.

### Here's how you do it in SPSS.
> ### First, paste part of the SPSS syntax found [here](http://www.ats.ucla.edu/stat/spss/seminars/Repeated_Measures/repeat.txt). Copy from "data list free / id exertype diet time1 time2 time3." to "end data." at the bottom of that data list.

### Compute a difference score (linear contrast = -1, 0, 1) and run a one-sample t-test.

COMPUTE Linear = time1 * -1 + time2 * 0 + time3 * 1

T-TEST

/ VAR Linear

/ TESTVAL 0.

### You can get the same results using the GLM command:

GLM Linear

/ INTERCEPT = INCLUDE

/ PRINT = PARAMETER.

### Commands used:
* COMPUTE
* T-TEST
* GLM
* INTERCEPT
* PRINT

### Here's how you do it in R.

### Read in the data.
> ### Below I read from the web comma-seperated data I prepared and uploaded to [my Bitbucket account](https://bitbucket.org/nmmichalak/). Then I print a random subset of this data so that you can see most or all of the conditions in this dataset.


```r
exerdiet <- read.csv(file = "https://bitbucket.org/nmmichalak/analysis-examples/raw/05db96c5c7e022b20c836e2729106e53ab239579/exercise_diet_example.csv", header = TRUE)

exerdiet[sample(x = 1:nrow(exerdiet),
                size = 10,
                replace = FALSE), ]
```

```
##    id exertype diet time1 time2 time3
## 18 18        2    2    92    96   101
## 8   8        1    2    92    94    95
## 27 27        3    2   100   126   140
## 25 25        3    1    94   110   116
## 5   5        1    1    91    92    91
## 30 30        3    2    99   111   150
## 2   2        1    1    90    92    93
## 6   6        1    2    83    83    84
## 21 21        3    1    93    98   110
## 10 10        1    2   100    97   100
```

### Functions used:
* [`read.csv()`](https://stat.ethz.ch/R-manual/R-devel/library/utils/html/read.table.html)
* [`sample()`](http://www.inside-r.org/r-doc/base/sample)
* [`nrow()`](http://www.inside-r.org/r-doc/base/nrow)
* I also use basic subsetting code. You can find an easy introductory tutorial for subsetting at [Quick-R](http://www.statmethods.net/management/subset.html).

### Convert exertype and diet variables into factor variables.


```r
exerdiet[,c("exertype","diet")] <- lapply(exerdiet[,c("exertype","diet")],
                                          factor)
```

### Functions used:
* [`lapply()`](http://www.inside-r.org/r-doc/base/lapply)
* [`factor()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/factor.html)
* [`c()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/c.html)

### Run a one-sample t-test on difference scores.


```r
t.test(x = as.matrix(
  exerdiet[,c("time1","time2","time3")]) %*% c(-1,0,1)
)
```

```
## 
## 	One Sample t-test
## 
## data:  as.matrix(exerdiet[, c("time1", "time2", "time3")]) %*% c(-1,     0, 1)
## t = 3.7418, df = 29, p-value = 0.0008026
## alternative hypothesis: true mean is not equal to 0
## 95 percent confidence interval:
##   5.123581 17.476419
## sample estimates:
## mean of x 
##      11.3
```

### You can get the same results using the `lm()` function:

```r
summary(
  lm(formula = as.matrix(
  exerdiet[,c("time1","time2","time3")]) %*% c(-1,0,1) ~ 1
)
)
```

```
## 
## Call:
## lm(formula = as.matrix(exerdiet[, c("time1", "time2", "time3")]) %*% 
##     c(-1, 0, 1) ~ 1)
## 
## Residuals:
##    Min     1Q Median     3Q    Max 
## -24.30 -10.30  -8.30   4.95  39.70 
## 
## Coefficients:
##             Estimate Std. Error t value Pr(>|t|)    
## (Intercept)    11.30       3.02   3.742 0.000803 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 16.54 on 29 degrees of freedom
```

> ### Instead of computing difference scores like I did in SPSS (totally valid way to do it), above I multiply a matrix of the three time columns by the standard linear contrast, -1, 0, 1. Put simply, each number in column 1 is multipled by -1, each number in column 2 is multipled by 0, and each number in column 3 is multipled by 1, and then the resulting rows are added together. The final result is one column of the difference scores. Here's a sample of what this looks like.


```r
head(head(as.matrix(
  exerdiet[,c("time1","time2","time3")]))
)
```

```
##      time1 time2 time3
## [1,]    85    85    88
## [2,]    90    92    93
## [3,]    97    97    94
## [4,]    80    82    83
## [5,]    91    92    91
## [6,]    83    83    84
```

```r
head(
  as.matrix(
  exerdiet[,c("time1","time2","time3")]) %*% c(-1, 0, 1)
)
```

```
##      [,1]
## [1,]    3
## [2,]    3
## [3,]   -3
## [4,]    3
## [5,]    0
## [6,]    1
```

### Functions used:
* [`t.test()`](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/t.test.html)
* [`as.matrix()`](http://www.inside-r.org/r-doc/base/as.matrix)
* [`%*%`](http://www.inside-r.org/r-doc/base/matmult)
* [`lm()`](http://www.inside-r.org/r-doc/stats/lm)
* [`summary()`](https://stat.ethz.ch/R-manual/R-devel/library/base/html/summary.html)
* [`head()`](http://www.inside-r.org/r-doc/utils/head)

> ### You'll notice that I didn't compute omnibus tests. Why? Because performing omnibus tests requires averaging error terms, and doing so with repeated measures data requires pretty restrictive assumptions about homogeneity of difference score variances. Put simply, if you computed the difference score between _every_ pair of levels in your within-subjects factor, assuming "homogeneity of treatment difference variances" means assuming all those variances are the same (i.e., homogeneous). When they're not and you go ahead and run an omnibus test anyway, you run the risk of inflating false-postive rates to 10% or even 15% (instead of the standard 5%). If, instead, you compute contrasts like in the above analyses, you can ignore this assumption because contrasts compare only two means, and the variance of only one difference score can't be heterogeneous with itself^2^.
> There are, of course, procedures for estimating how much your data deviates from homogeneity and adjusting the degrees of freedom associated with the omnibus F test. I may rant about this topic in more detail in later posts.

> ### Happy R,
>
> ### Nick

#### Footnotes
1. By the way, UCLA's IDRE is an excellent source for in-depth statistics tutorials.
2. See Chapters 11-13 of Maxwell, S. E., & Delaney, H. D. (2004). Designing experiments and analyzing data: A model comparison perspective. New York, NY: Psychology Press. Richard Gonzalez of the University of Michigan summarizes this way of thinking in his Advanced topics in ANOVA lecture notes [here](http://www-personal.umich.edu/~gonzo/coursenotes/file5.pdf).
