DATA LIST FILE= "LooksOrPersonality.csv"  free (",")
/ participant gender att_high av_high ug_high att_some av_some ug_some att_none av_none ug_none  .

VARIABLE LABELS
participant "participant" 
 gender "gender" 
 att_high "att_high" 
 av_high "av_high" 
 ug_high "ug_high" 
 att_some "att_some" 
 av_some "av_some" 
 ug_some "ug_some" 
 att_none "att_none" 
 av_none "av_none" 
 ug_none "ug_none" 
 .

VALUE LABELS
/
participant 
1 "P01" 
 2 "P02" 
 3 "P03" 
 4 "P04" 
 5 "P05" 
 6 "P06" 
 7 "P07" 
 8 "P08" 
 9 "P09" 
 10 "P10" 
 11 "P11" 
 12 "P12" 
 13 "P13" 
 14 "P14" 
 15 "P15" 
 16 "P16" 
 17 "P17" 
 18 "P18" 
 19 "P19" 
 20 "P20" 
/
gender 
1 "Female" 
 2 "Male" 
.

EXECUTE.
