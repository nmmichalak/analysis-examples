DATA LIST FILE= "fake.long.csv"  free (",")
/ id picture disgust  .

VARIABLE LABELS
id "id" 
 picture "picture" 
 disgust "disgust" 
 .

VALUE LABELS
/
picture 
1 "Low Disgust" 
 2 "Moderate Disgust" 
 3 "High Disgust" 
.

EXECUTE.
