DATA LIST FILE= "bushtucker.csv"  free (",")
/ participant stick_insect kangaroo_testicle fish_eye witchetty_grub  .

VARIABLE LABELS
participant "participant" 
 stick_insect "stick_insect" 
 kangaroo_testicle "kangaroo_testicle" 
 fish_eye "fish_eye" 
 witchetty_grub "witchetty_grub" 
 .

VALUE LABELS
/
participant 
1 "P1" 
 2 "P2" 
 3 "P3" 
 4 "P4" 
 5 "P5" 
 6 "P6" 
 7 "P7" 
 8 "P8" 
.

EXECUTE.
