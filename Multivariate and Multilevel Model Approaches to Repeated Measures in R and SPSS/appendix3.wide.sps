DATA LIST FILE= "appendix3.wide.csv"  free (",")
/ Subject Recall.Neg Recall.Neu Recall.Pos  .

VARIABLE LABELS
Subject "Subject" 
 Recall.Neg "Recall.Neg" 
 Recall.Neu "Recall.Neu" 
 Recall.Pos "Recall.Pos" 
 .

VALUE LABELS
/
Subject 
1 "Faye" 
 2 "Jason" 
 3 "Jim" 
 4 "Ron" 
 5 "Victor" 
.

EXECUTE.
