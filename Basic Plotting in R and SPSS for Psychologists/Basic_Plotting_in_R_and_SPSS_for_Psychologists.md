# Basic Plotting in R and SPSS for Psychologists

# fake between-subjects data


```r
set.seed(1)
faked <- data.frame(factor1 = rep(x = LETTERS[1:2],
                                  each = 50),
                    factor2 = rep(x = LETTERS[3:4],
                                  each = 25,
                                  times = 4),
                    outcome = c(rnorm(n = 25, mean = 4, sd = 1),
                                rnorm(n = 25, mean = 3, sd = 1),
                                rnorm(n = 25, mean = 4, sd = 1),
                                rnorm(n = 25, mean = 5, sd = 1))
)
```

# boxplots for factor 1


```r
boxplot(outcome ~ factor1, data = faked,
        ylab = "Outcome",
        xlab = "Factor 1")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

# plot main effect for factor 1


```r
library(ggplot2)
ggplot(data = faked, aes(x = factor1, y = outcome, fill = factor1)) +
  stat_summary(fun.data = "mean_se", geom = "bar") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  labs(x = "Factor 1", y = "Outcome", fill = "Factor 1")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

# boxplots for factor 2


```r
boxplot(outcome ~ factor2, data = faked,
        ylab = "Outcome",
        xlab = "Factor 2")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

# plot main effect for factor 2


```r
ggplot(data = faked, aes(x = factor2, y = outcome, fill = factor2)) +
  stat_summary(fun.data = "mean_se", geom = "bar") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  labs(x = "Factor 2", y = "Outcome", fill = "Factor 2")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

# boxplots for factor 1 x factor 2


```r
boxplot(outcome ~ interaction(factor1, factor2, sep = " x "), data = faked,
        ylab = "Outcome",
        xlab = "Group")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

# plot interaction between factor 1 and factor 2


```r
ggplot(data = faked, aes(x = factor1, y = outcome, fill = factor2)) +
  stat_summary(fun.data = "mean_se", geom = "bar", position = position_dodge(1)) +
  stat_summary(fun.data = "mean_se", geom = "errorbar", position = position_dodge(1), width = 0.1) +
  labs(x = "Factor 1", y = "Outcome", fill = "Factor 2")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

# fake between-subjects and within-subjects data


```r
library(MASS)
set.seed(1)

# group A, .6 correlation between times, mean of time1 = 0 and time2 = -1
A <- mvrnorm(n = 100,
                  mu = c(4, 3),
                  Sigma = matrix(data = c(1,.5,
                                          .5,1),
                                 nrow = 2,
                                 ncol = 2,
                                 byrow = TRUE))

# group A, .6 correlation between times, mean of time1 = 1 and time2 = 2
B <- mvrnorm(n = 100,
                  mu = c(4, 5),
                  Sigma = matrix(data = c(1,.5,
                                          .5,1),
                                 nrow = 2,
                                 ncol = 2,
                                 byrow = TRUE))

time <- data.frame(rbind(A, B))
names(time) <- c("time1","time2")
```

# plot time 1 versus time 2


```r
# average value
outcome.avg <- mean(x = as.matrix(x = time))

# subject average
time$subj.avg <- rowMeans(time)

# remove within-subject variability from time 1
time$time1.new <- with(time, time1 - subj.avg + outcome.avg)

# remove within-subject variability from time 2
time$time2.new <- with(time, time2 - subj.avg + outcome.avg)

# long format
faked2 <- with(time, data.frame(id = c(1:200,1:200),
                                  factor1 = rep(LETTERS[1:2],
                                                each = 100,
                                                times = 2),
                                  time = rep(c("time 1","time 2"),
                                               each = 200),
                                  outcome = c(time1.new, time2.new)))

# now plot
ggplot(data = faked2,
       aes(x = time, y = outcome, fill = time)) +
  stat_summary(fun.data = "mean_se", geom = "bar") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  labs(x = "time", y = "Outcome", fill = "time")  
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

# boxplots for time 1 and time 2


```r
boxplot(outcome ~ time, data = faked2,
        ylab = "Outcome",
        xlab = "Time",
        names = c("Time 1", "Time 2"))
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-10-1.png)<!-- -->

# Plot within-subjects x between-subjects interaction


```r
ggplot(data = faked2,
       aes(x = time, y = outcome, fill = factor1)) +
  stat_summary(fun.data = "mean_se", geom = "bar", position = position_dodge(1)) +
  stat_summary(fun.data = "mean_se", geom = "errorbar", position = position_dodge(1), width = 0.1) +
  labs(x = "time", y = "Outcome", fill = "Factor 1")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-11-1.png)<!-- -->

# boxplots for time 1 and time 2


```r
boxplot(outcome ~ interaction(time, factor1, sep = " x "), data = faked2,
        ylab = "Outcome",
        xlab = "Time")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-12-1.png)<!-- -->

# think of these data differently (effects of time 1 and time 2 depend on factor 1)


```r
# add groups to the time dataframe from before the transformations
time$factor1 <- rep(x = LETTERS[1:2], each = 100)

ggplot(time, aes(x = time1, time2, color = factor1)) +
  geom_point() +
  geom_smooth(method = "lm") +
  labs(x = "Time 1", y = "Time 2", color = "Factor 1")
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-13-1.png)<!-- -->

# changing colors (fivethirtyeight theme)


```r
library(devtools)
devtools::install_github('bart6114/artyfarty', force = TRUE)
```

```
## Downloading GitHub repo bart6114/artyfarty@master
## from URL https://api.github.com/repos/bart6114/artyfarty/zipball/master
```

```
## Installing artyfarty
```

```
## '/Library/Frameworks/R.framework/Resources/bin/R' --no-site-file  \
##   --no-environ --no-save --no-restore --quiet CMD INSTALL  \
##   '/private/var/folders/lt/j58zn6_55k3c1ds4chznyx100000gn/T/RtmpxWqTxt/devtools17e5a343d0ad8/Bart6114-artyfarty-abf831d'  \
##   --library='/Library/Frameworks/R.framework/Versions/3.3/Resources/library'  \
##   --install-tests
```

```
## 
```

```r
library(artyfarty)
ggplot(data = faked2,
       aes(x = time, y = outcome, fill = factor1)) +
  stat_summary(fun.data = "mean_se", geom = "bar", position = position_dodge(1)) +
  stat_summary(fun.data = "mean_se", geom = "errorbar", position = position_dodge(1), width = 0.1) +
  labs(x = "time", y = "Outcome", fill = "Factor 1") +
  theme_five38() +
  scale_fill_manual(values = pal("five38"))
```

![](Basic_Plotting_in_R_and_SPSS_for_Psychologists_files/figure-html/unnamed-chunk-14-1.png)<!-- -->

