# Reproducing Hayes’ PROCESS Model 1: Dichotomous Moderator
Nick Michalak  
11/18/2016  

# fake data


```r
library(MASS)

# set seed so that you can reproduce the same numbers
set.seed(1)

# 1000 observations
# 500 control, 500 treatment
# positive relationship in with moderator control condition (r = .5); mean = 1
# negative relationship in with moderator control condition (r = -.5); mean = 0

fake <- data.frame(id = 1:1000,
                   cond = rep(x = c("Control","Treatment"), each = 500),
                   rbind(mvrnorm(n = 500, mu = c(1,1), Sigma = diag(x = .5, nrow = 2, ncol = 2) + .5),
                         mvrnorm(n = 500, mu = c(0,0), Sigma = diag(x = 1.5, nrow = 2, ncol = 2) - .5)))

# center X1
fake$X1.c <- with(data = fake, as.numeric(scale(x = X1, center = T, scale = F)))

# effect code condition variable
fake$cond.e <- with(data = fake, ifelse(cond == "Control", -1, 1))

# create numeric interaction variable
fake$cond.X1.int <- with(data = fake, cond.e*X1.c)
```

# plot relationship


```r
library(ggplot2)

## devtools::install_github('bart6114/artyfarty',force=TRUE)
library(artyfarty)

ggplot(data = fake, aes(x = X1.c, y = X2, color = cond)) +
  geom_point() +
  geom_smooth(method = "lm") +
  scale_y_continuous(breaks = seq(-5,5,1), limits = c(-5,5)) +
  labs(x = "X1 (Centered)", y = "X2", color = "Condition") +
  theme_five38() +
  scale_color_manual(values = pal("five38")) +
  theme(legend.position = "top")
```

![](Reproducing_Hayes_PROCESS_Model_1_with_Dichotomous_Moderator_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

# test moderation and simple slopes using lavaan


```r
library(lavaan)

# write model
fake.mod1 <- '# regressions
                X2 ~ b1*X1.c
                X2 ~ b2*cond.e
                X2 ~ b3*cond.X1.int

              # simple slopes
              # cond == -1
                control := b1 + b3*-1

              # cond == 0
                center := b1 + b3*0

              # cond == 1
                treatment := b1 + b3*1'

# fit model
mod1.fit <- sem(model = fake.mod1,
            data = fake,
            se = "bootstrap",
            bootstrap = 1000)

# summarize
summary(mod1.fit,
        fit.measures = TRUE,
        standardize = TRUE,
        rsquare = TRUE)
```

```
## lavaan (0.5-22) converged normally after  13 iterations
## 
##   Number of observations                          1000
## 
##   Estimator                                         ML
##   Minimum Function Test Statistic                0.000
##   Degrees of freedom                                 0
## 
## Model test baseline model:
## 
##   Minimum Function Test Statistic              449.669
##   Degrees of freedom                                 3
##   P-value                                        0.000
## 
## User model versus baseline model:
## 
##   Comparative Fit Index (CFI)                    1.000
##   Tucker-Lewis Index (TLI)                       1.000
## 
## Loglikelihood and Information Criteria:
## 
##   Loglikelihood user model (H0)              -5632.756
##   Loglikelihood unrestricted model (H1)      -5632.756
## 
##   Number of free parameters                          4
##   Akaike (AIC)                               11273.512
##   Bayesian (BIC)                             11293.143
##   Sample-size adjusted Bayesian (BIC)        11280.438
## 
## Root Mean Square Error of Approximation:
## 
##   RMSEA                                          0.000
##   90 Percent Confidence Interval          0.000  0.000
##   P-value RMSEA <= 0.05                             NA
## 
## Standardized Root Mean Square Residual:
## 
##   SRMR                                           0.000
## 
## Parameter Estimates:
## 
##   Information                                 Observed
##   Standard Errors                            Bootstrap
##   Number of requested bootstrap draws             1000
##   Number of successful bootstrap draws            1000
## 
## Regressions:
##                    Estimate  Std.Err  z-value  P(>|z|)   Std.lv  Std.all
##   X2 ~                                                                  
##     X1.c      (b1)   -0.004    0.028   -0.137    0.891   -0.004   -0.004
##     cond.e    (b2)   -0.494    0.032  -15.427    0.000   -0.494   -0.438
##     cnd.X1.nt (b3)   -0.454    0.028  -16.423    0.000   -0.454   -0.415
## 
## Variances:
##                    Estimate  Std.Err  z-value  P(>|z|)   Std.lv  Std.all
##    .X2                0.812    0.034   23.891    0.000    0.812    0.638
## 
## R-Square:
##                    Estimate
##     X2                0.362
## 
## Defined Parameters:
##                    Estimate  Std.Err  z-value  P(>|z|)   Std.lv  Std.all
##     control           0.450    0.039   11.418    0.000    0.450    0.411
##     center           -0.004    0.028   -0.137    0.891   -0.004   -0.004
##     treatment        -0.458    0.039  -11.621    0.000   -0.458   -0.419
```

```r
# bootstrapped estimates
parameterEstimates(mod1.fit,
                   boot.ci.type = "bca.simple",
                   level = .95,
                   ci = TRUE,
                   standardized = FALSE)
```

```
##            lhs op         rhs     label    est    se       z pvalue
## 1           X2  ~        X1.c        b1 -0.004 0.028  -0.137  0.891
## 2           X2  ~      cond.e        b2 -0.494 0.032 -15.427  0.000
## 3           X2  ~ cond.X1.int        b3 -0.454 0.028 -16.423  0.000
## 4           X2 ~~          X2            0.812 0.034  23.891  0.000
## 5         X1.c ~~        X1.c            1.326 0.000      NA     NA
## 6         X1.c ~~      cond.e           -0.513 0.000      NA     NA
## 7         X1.c ~~ cond.X1.int           -0.020 0.000      NA     NA
## 8       cond.e ~~      cond.e            1.000 0.000      NA     NA
## 9       cond.e ~~ cond.X1.int            0.000 0.000      NA     NA
## 10 cond.X1.int ~~ cond.X1.int            1.063 0.000      NA     NA
## 11     control :=    b1+b3*-1   control  0.450 0.039  11.418  0.000
## 12      center :=     b1+b3*0    center -0.004 0.028  -0.137  0.891
## 13   treatment :=     b1+b3*1 treatment -0.458 0.039 -11.621  0.000
##    ci.lower ci.upper
## 1    -0.059    0.049
## 2    -0.563   -0.433
## 3    -0.509   -0.401
## 4     0.750    0.886
## 5     1.326    1.326
## 6    -0.513   -0.513
## 7    -0.020   -0.020
## 8     1.000    1.000
## 9     0.000    0.000
## 10    1.063    1.063
## 11    0.371    0.524
## 12   -0.059    0.049
## 13   -0.534   -0.378
```

```r
# write data to file
setwd("~/Desktop/analysis-examples/Reproducing Hayes's Model 1 with a Dichotomous Moderator/")
write.csv(x = fake, file = "fake.csv", row.names = FALSE)
```

