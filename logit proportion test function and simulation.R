# create logit test function
logit.test <- function(ps,ns,cont){
  ihat = sum(log(ps/(1-ps))*cont)
  se = sqrt(sum(1/(ns*ps*(1-ps))))
  z = ihat/se
  p.value = 1 - pnorm(z)
  return(list(ihat = ihat, se = se, z = z, p.value = p.value))
}

# test it with a 4 proportion-long linear contrast
logit.test(ps = c(.2,.3,.4,.5), ns = c(100,100,100,100), cont = contr.poly(4)[,1])

# function for testing null effect between equal proportions
# returns the p-value
logit.sim.fun <- function(n){
  p1 = sample(x = c(0,1), size = n, replace = TRUE, prob = c(.95,.05))
  p2 = sample(x = c(0,1), size = n, replace = TRUE, prob = c(.95,.05))
  test = logit.test(ps = c(mean(p1),mean(p2)), ns = c(length(p1),length(p2)), cont = c(-1,1))
  p = test$p.value
  return(p)
}

# apply this over a sequence of n (sample sizes per proportion)
# 10000 simulations
# find error rate
sapply(seq(5,100,5), function(x) mean(replicate(n = 10000, expr = logit.sim.fun(100), simplify = TRUE) < .05), simplify = TRUE)  
mean(replicate(n = 10000, expr = logit.sim.fun(100), simplify = TRUE) < .05)

# do the same, but test 80% power
# find true difefrence of 25%
library(pwr)
sin(.5)^2
pwr.p.test()